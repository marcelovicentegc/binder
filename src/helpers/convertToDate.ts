export const convertToDate = (date: string) => {
  const parts = date.split("/")
  const year = Number(parts[2])
  const month = Number(parts[1]) - 1
  const day = Number(parts[0])

  if (
    year < 1900 ||
    year > new Date().getFullYear() ||
    month < 0 ||
    month > 12
  ) {
    return NaN
  }

  return new Date(year, month, day, 0, 0, 0, 0)
}

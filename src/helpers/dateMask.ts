export const dateMask = (date: string) => {
  date = date.replace(/[^0-9]/g, "")
  date = date.replace(/(\d{2})(\d)/, "$1/$2")
  date = date.replace(/(\d{2})(\d)/, "$1/$2")
  date = date.replace(/(\d{4})(\d)/, "$1")
  date = date.substring(0, 10)
  return date
}

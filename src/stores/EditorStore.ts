import { decorate, observable, action } from "mobx"
import { RootStore } from "./RootStore"

export class EditorStore {
  protected rootStore: RootStore

  public textContent: string

  public constructor(rootStore: RootStore) {
    this.rootStore = rootStore
  }

  public setTextContent = (textContent: string) => {
    this.textContent = textContent
  }
}

decorate(EditorStore, {
  textContent: observable,
  setTextContent: action,
})

import { decorate, observable, action } from "mobx"
import { RootStore } from "./RootStore"

export interface IUser {
  fullName: string
  birthdate: string
  gender: string
  email: string
  password: string
  nickname: string
  school: ISchool
}

export interface ISchool {
  name: string
  grade: EGrade | ""
  class: string
  shift: EShift | ""
}

export enum EGrade {
  first = "1",
  second = "2",
  third = "3",
  fourth = "4",
  fifth = "5",
  sixth = "6",
  seventh = "7",
  eighth = "8",
  ninth = "9",
  tenth = "10",
  eleventh = "11",
  twelfth = "12",
}

export enum EShift {
  morning = "morning",
  afternoon = "afternoon",
}

export class UserStore {
  protected rootStore: RootStore
  public user: IUser = {
    fullName: "Maria Yolanda",
    birthdate: "01/01/2005",
    gender: "woman",
    email: "example@email.com",
    nickname: "myolanda",
    password: "123456789",
    school: {
      name: "Escola Nelson Madela",
      grade: EGrade.eighth,
      class: "a",
      shift: EShift.morning,
    },
  }

  public constructor(rootStore: RootStore) {
    this.rootStore = rootStore
  }

  public setUser = (user: IUser) => {
    this.user = user
  }

  public clearUser = () => {
    this.user = {
      fullName: "",
      birthdate: "",
      gender: "",
      email: "",
      nickname: "",
      password: "",
      school: {
        name: "",
        grade: "",
        class: "",
        shift: "",
      },
    }
  }
}

decorate(UserStore, {
  user: observable,
  setUser: action,
  clearUser: action,
})

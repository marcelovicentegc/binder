import { IntroStore, EditorStore, BinderStore, UserStore } from "."
import { createContext } from "react"

export class RootStore {
  public introStore: IntroStore
  public editorStore: EditorStore
  public binderStore: BinderStore
  public userStore: UserStore

  public constructor() {
    this.introStore = new IntroStore(this)
    this.editorStore = new EditorStore(this)
    this.binderStore = new BinderStore(this)
    this.userStore = new UserStore(this)

    return {
      introStore: this.introStore,
      editorStore: this.editorStore,
      binderStore: this.binderStore,
      userStore: this.userStore,
    }
  }
}

export { ECurrentStep } from "."
export const rootStore = new RootStore()
export const rootStoreContext = createContext(rootStore)

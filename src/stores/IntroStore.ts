import { decorate, observable, action } from "mobx"
import { RootStore } from "./RootStore"

export enum ECurrentStep {
  login,
  intro1,
  intro2,
  intro3,
  intro4,
  skip,
}

export class IntroStore {
  protected rootStore: RootStore
  public currentStep: ECurrentStep = ECurrentStep.login

  public constructor(rootStore: RootStore) {
    this.rootStore = rootStore
  }

  public setCurrentStep = (currentStep: ECurrentStep) => {
    this.currentStep = currentStep
  }
}

decorate(IntroStore, {
  currentStep: observable,
  setCurrentStep: action,
})

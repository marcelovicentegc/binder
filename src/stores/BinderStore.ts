import naturalSciencesBg from "../assets/images/naturalSciencesBg.png"
import geometryBg from "../assets/images/geometryBg.png"
import ancientHistoryBg from "../assets/images/ancientHistoryBg.png"
import portugueseLanguageBg from "../assets/images/portugueseLanguageBg.png"
import pinkLeatherBg from "../assets/images/pinkLeatherBg.png"
import greenLeatherBg from "../assets/images/greenLeatherBg.png"
import blueLeatherBg from "../assets/images/blueLeatherBg.png"
import lilacLeatherBg from "../assets/images/lilacLeatherBg.png"
import Fuse from "fuse.js"

import { decorate, observable, action, runInAction, toJS } from "mobx"
import { RootStore } from "./RootStore"
import { theme, BinderInterface, BoardInterface } from "@binder/ui"
import { navigate } from "gatsby"

export interface IGenericItem {
  key: string
  fk?: string
  title: string
  description: string
  createdAt: string
  onClick?: () => void
}

const binderSearchOptions = {
  keys: [
    "title.text",
    "courseInformation.grade",
    "courseInformation.teacher",
    "courseInformation.schedule",
  ],
}

const boardSearchOptions = {
  keys: ["title"],
}

const philosophyBinder: BinderInterface = {
  key: "5",
  img: blueLeatherBg,
  title: { color: "#E2873C", text: "Filosofia" },
  desc: { color: "#F5F5F5", text: "Hermenêutica" },
  date: new Date(2020, 1, 3),
  courseInformation: {
    grade: "8º ano",
    teacher: "Prof. Ana Skogen",
    schedule: "qui 10h30 - 11:20",
  },
  boards: [
    {
      key: "1124",
      fk: "5",
      date: new Date(2020, 1, 2),
      title: "A hermenêutica do sujeito",
      snapshot: "",
    },
    {
      key: "643122",
      fk: "5",
      date: new Date(2020, 1, 1),
      title: "Teoria da interpretação",
      snapshot: "",
      sharedWith: ["João", "Maria", "Isidoro"],
      lastUpdatedAt: "última atualização ontem, às 21:38, por você",
    },
    {
      key: "365734",
      fk: "5",
      date: new Date(2020, 1, 13),
      title: "A vida e obra de Heidegger",
      snapshot: "",
      sharedWith: ["Luíza"],
      lastUpdatedAt: "última atualização ontem, às 11:05, por Luíza",
    },
  ],
}

export class BinderStore {
  protected rootStore: RootStore

  public binders: BinderInterface[] = [
    {
      key: "1",
      img: naturalSciencesBg,
      title: { color: "white", text: "Ciências" },
      desc: { color: theme.color.white1, text: "Reino Plantae" },
      courseInformation: {
        grade: "7º ano",
        teacher: "Prof. Moiséis Mira",
        schedule: "qua 10h30 - 11:20",
      },
      date: new Date(2020, 1, 12),
      boards: [
        {
          key: "17635",
          fk: "1",
          date: new Date(2020, 1, 12),
          title: "Embryophytas: plantas terrestres",
          snapshot: "",
        },
        {
          key: "20120",
          fk: "1",
          date: new Date(2020, 1, 11),
          title: "Chlorophyta: algas verdes",
          snapshot: "",
          sharedWith: ["João", "Maria", "Isidoro"],
          lastUpdatedAt: "última atualização ontem, às 21:38, por você",
        },
      ],
    },
    {
      key: "2",
      img: ancientHistoryBg,
      title: { color: theme.color.white1, text: "História" },
      desc: { color: theme.color.white1, text: "Antiga" },
      date: new Date(2020, 1, 11),
      courseInformation: {
        grade: "6º ano",
        teacher: "Prof. Jorge France",
        schedule: "qua 07:30 - 09:40",
      },
    },
    {
      key: "3",
      img: geometryBg,
      backgroundColor: theme.color.green6,
      title: { color: theme.color.yellow5, text: "Geometria" },
      desc: { color: "#fff", text: "Introdução" },
      date: new Date(2019, 1, 11),
      courseInformation: {
        grade: "7º ano",
        teacher: "Prof. Alécio Mascarenhas",
        schedule: "seg 16:30 - 18:00",
      },
    },
    {
      key: "4",
      img: lilacLeatherBg,
      title: { color: theme.color.yellow2, text: "Geografia" },
      desc: { color: theme.color.white3, text: "Física" },
      courseInformation: {
        grade: "7º ano",
        teacher: "Prof. Apolinário",
        schedule: "qui 10h30 - 13hc0",
      },
      backgroundColor: theme.color.purple1,
      date: new Date(2020, 1, 8),
    },
    philosophyBinder,
  ]
  public archivedBoards: BoardInterface[] = [
    {
      key: "12142",
      fk: "7",
      date: new Date(2020, 1, 2),
      title: "Relações harmônicas",
      snapshot: "",
    },
    {
      key: "152461432",
      fk: "6",
      date: new Date(2020, 1, 2),
      title: "Advérbio",
      snapshot: "",
    },
    {
      key: "191281432",
      fk: "6",
      date: new Date(2020, 1, 2),
      title: "Substantivos",
      snapshot: "",
    },
    {
      key: "924115269",
      fk: "5",
      date: new Date(2020, 1, 2),
      title: "Pós modernismo na cultura",
      snapshot: "",
    },
  ]
  public archivedBinders: BinderInterface[] = [
    {
      key: "6",
      img: portugueseLanguageBg,
      title: { color: "#1A6882", text: "Português" },
      desc: { color: "#ED5653", text: "Morfologia" },
      date: new Date(2020, 1, 3),
    },
    {
      key: "7",
      img: greenLeatherBg,
      title: { color: "#59B5CF", text: "Ciências" },
      desc: { color: "#F5F5F5", text: "Ecologia" },
      date: new Date(2020, 1, 3),
    },
    {
      key: "8",
      img: pinkLeatherBg,
      title: { color: "#EBA248", text: "Inglês" },
      desc: { color: "#F5F5F5", text: "Writing" },
      date: new Date(2020, 1, 3),
    },
  ]
  public deletedBoards: BoardInterface[] = []
  public deletedBinders: BinderInterface[] = []
  public queriedItems: IGenericItem[] = []
  public selectedBinder: BinderInterface | null = null
  public displayEditBinderModal = false
  public displayAddBinderModal = false
  public displayEditBinderStyleModal = false
  public cameFromSearch = false

  public constructor(rootStore: RootStore) {
    this.rootStore = rootStore
  }

  private getBoards = () => {
    let boards: BoardInterface[] = []

    this.binders.map(binder => boards.concat(binder.boards))

    return boards
  }

  public bindersQuery = new Fuse(this.binders, binderSearchOptions)
  public boardsQuery = new Fuse(this.getBoards(), boardSearchOptions)

  public setSelectedBinder = (selectedBinder: BinderInterface) => {
    this.selectedBinder = selectedBinder
  }

  public setDisplayEditBinderModal = (flag: boolean) => {
    this.displayEditBinderModal = flag
  }

  public setDisplayEditBinderStyleModal = (flag: boolean) => {
    this.displayEditBinderStyleModal = flag
  }

  public setDisplayAddBinderModal = (flag: boolean) => {
    this.displayAddBinderModal = flag
  }

  public addBinder = (binder: BinderInterface) => {
    this.binders.push(binder)
  }

  public updateBinder = (b: BinderInterface) => {
    const binderIndex = this.binders.findIndex(binder => binder.key === b.key)
    this.binders[binderIndex] = b
    this.selectedBinder = b
  }

  public setCameFromSearch = (flag: boolean) => {
    this.cameFromSearch = flag
  }

  public deleteBinder = (key: string) => {
    const binderIndex = this.binders.findIndex(binder => binder.key === key)

    const binder = this.binders[binderIndex]

    if (binder.boards) {
      binder.boards.map(board => this.deletedBoards.push(board))
    }

    this.binders[binderIndex].boards = []

    this.deletedBinders.push(binder)
    this.binders.splice(binderIndex, 1)
  }

  public archiveBinder = (key: string) => {
    const binderIndex = this.binders.findIndex(binder => binder.key === key)

    const binder = this.binders[binderIndex]

    if (binder.boards) {
      binder.boards.map(board => {
        this.archiveBoard(board.key, binder)
      })
    }

    this.archivedBinders.push(binder)
    this.binders.splice(binderIndex, 1)
  }

  private removeAndGetBoardFromBinder = runInAction(
    () => (key: string, currentBinder: BinderInterface) => {
      const boardIndex = currentBinder.boards.findIndex(
        board => board.key === key
      )

      const board = currentBinder.boards[boardIndex]

      currentBinder.boards.splice(boardIndex, 1)

      return board
    }
  )

  public deleteBoard = (key: string, currentBinder: BinderInterface) => {
    const board = this.removeAndGetBoardFromBinder(key, currentBinder)
    this.deletedBoards.push(board)
  }

  public definitelyDeleteBoards = () => {
    this.deletedBinders = []
    this.deletedBoards = []
  }

  public definitelyDeleteBoardsByKeys = (keys: string[]) => {
    keys.map(key => {
      const binderIndex = this.deletedBinders.findIndex(
        binder => binder.key === key
      )

      if (binderIndex > -1) {
        const boards = this.deletedBoards.filter(boards => boards.fk === key)

        if (boards) {
          this.deletedBoards.map((board, boardIndex) => {
            boards.map(boardToDelete => {
              if (boardToDelete.key === board.key) {
                this.deletedBoards.splice(boardIndex, 1)
              }
            })
          })
        }

        this.deletedBoards.splice(binderIndex, 1)
      } else {
        const boardIndex = this.deletedBoards.findIndex(
          board => board.key === key
        )

        if (boardIndex > -1) {
          this.deletedBoards.splice(boardIndex, 1)
        }
      }
    })
  }

  public archiveBoard = (key: string, currentBinder: BinderInterface) => {
    const board = this.removeAndGetBoardFromBinder(key, currentBinder)
    this.archivedBoards.push(board)
  }

  public recoverBoardsFromArchiveByKeys = (keys: string[]) => {
    keys.map(key => {
      let binderIndex = this.archivedBinders.findIndex(
        binder => binder.key === key
      )

      if (binderIndex !== -1) {
        // There is a Binder to delete
        const binder = this.archivedBinders[binderIndex]

        // Remove binder from the archived
        // binders section
        this.archivedBinders.splice(binderIndex, 1)

        // Push retrieved binder to
        // existing binders set
        this.binders.push(binder)

        // Recover every board associated
        // with this binder that might be
        // archived as well
        if (binder.boards) {
          binder.boards.map(board => {
            this.archivedBoards.map((archivedBoard, archivedBoardIndex) => {
              if (board.key === archivedBoard.key) {
                // Remove board from archived boards
                // section
                this.archivedBoards.splice(archivedBoardIndex, 1)

                // Push board to corresponding binder,
                // which is at the end of the array
                this.binders[this.binders.length].boards.push(board)
              }
            })
          })
        }
      } else {
        // This key did not belong to a binder,
        // thus we'll look for a board with a
        // matching key
        const boardIndex = this.archivedBoards.findIndex(
          board => board.key === key
        )

        if (boardIndex !== -1) {
          const board = this.archivedBoards[boardIndex]

          if (board) {
            this.archivedBoards.splice(boardIndex, 1)

            // Let's look for where this board
            // belongs
            binderIndex = this.binders.findIndex(
              binder => binder.key === board.fk
            )

            // This board belongs to
            // a binder that is currently
            // on the active binders set
            if (binderIndex > -1) {
              if (this.binders[binderIndex].boards) {
                this.binders[binderIndex].boards.push(board)
              } else {
                this.binders[binderIndex].boards = [board]
              }

              return
            }

            // This board belongs to
            // a binder that is currently
            // on the archived binders set
            binderIndex = this.archivedBinders.findIndex(
              binder => binder.key === board.fk
            )

            if (binderIndex > -1) {
              // Push that binder to the active
              // binders set
              this.binders.push(this.archivedBinders[binderIndex])

              // Remove this binder from the archived
              // binders set
              this.archivedBinders.splice(binderIndex, 1)

              // Push board to its corresponding
              // binder, which is the last one
              // from the active binders set
              binderIndex = this.binders.findIndex(
                binder => binder.key === board.fk
              )

              if (binderIndex > -1) {
                if (this.binders[binderIndex].boards) {
                  this.binders[binderIndex].boards.push(board)
                } else {
                  this.binders[binderIndex].boards = [board]
                }
              }
            }
          }
        }
      }
    })
  }

  public recoverBoardsFromTrashByKeys = (keys: string[]) => {
    keys.map(key => {
      let binderIndex = this.deletedBinders.findIndex(
        binder => binder.key === key
      )

      if (binderIndex !== -1) {
        const binder = this.deletedBinders[binderIndex]

        this.deletedBinders.splice(binderIndex, 1)

        this.binders.push(binder)

        if (binder.boards) {
          binder.boards.map(board => {
            this.deletedBoards.map((deletedBoard, deletedBoardIndex) => {
              if (board.key === deletedBoard.key) {
                this.deletedBoards.splice(deletedBoardIndex, 1)

                this.binders[this.binders.length].boards.push(board)
              }
            })
          })
        }
      } else {
        const boardIndex = this.deletedBoards.findIndex(
          board => board.key === key
        )

        if (boardIndex !== -1) {
          const board = this.deletedBoards[boardIndex]

          if (board) {
            this.deletedBoards.splice(boardIndex, 1)

            binderIndex = this.binders.findIndex(
              binder => binder.key === board.fk
            )

            if (binderIndex > -1) {
              if (this.binders[binderIndex].boards) {
                this.binders[binderIndex].boards.push(board)
              } else {
                this.binders[binderIndex].boards = [board]
              }

              return
            }

            binderIndex = this.deletedBinders.findIndex(
              binder => binder.key === board.fk
            )

            if (binderIndex > -1) {
              this.binders.push(this.deletedBinders[binderIndex])

              this.deletedBinders.splice(binderIndex, 1)

              binderIndex = this.binders.findIndex(
                binder => binder.key === board.fk
              )

              if (binderIndex > -1) {
                if (this.binders[binderIndex].boards) {
                  this.binders[binderIndex].boards.push(board)
                } else {
                  this.binders[binderIndex].boards = [board]
                }
              }
            }
          }
        }
      }
    })
  }

  public recoverBoardsFromTrash = () => {
    if (this.deletedBinders.length > 0) {
      this.deletedBinders.map(binder => {
        const boards = this.deletedBoards.filter(
          board => board.fk === binder.key
        )

        if (boards) {
          binder.boards = boards
          this.binders.push(binder)
        }
      })
    } else {
      this.deletedBoards.map(board => {
        const binderIndex = this.binders.findIndex(
          binder => binder.key === board.fk
        )

        if (this.binders[binderIndex]) {
          this.binders[binderIndex].boards.push(board)
        }
      })
    }

    this.deletedBinders = []
    this.deletedBoards = []
  }

  public recoverBoardsFromArchive = () => {
    if (this.archivedBinders.length > 0) {
      this.archivedBinders.map(binder => {
        const boards = this.archivedBoards.filter(
          board => board.fk === binder.key
        )

        if (boards) {
          binder.boards = boards
          this.binders.push(binder)
        }
      })
    } else {
      this.archivedBoards.map(board => {
        const binderIndex = this.binders.findIndex(
          binder => binder.key === board.fk
        )

        if (this.binders[binderIndex]) {
          this.binders[binderIndex].boards.push(board)
        }
      })
    }

    this.archivedBinders = []
    this.archivedBoards = []
  }

  public search = (query: string) => {
    if (query.length === 1) {
      this.queriedItems = []
      this.setCameFromSearch(false)
      return
    }

    this.boardsQuery.search(query).map(board => {
      if (this.queriedItems.findIndex(item => item.key === board.key) === -1) {
        this.queriedItems.push({
          title: board.title,
          key: board.key,
          description: "",
          createdAt: "",
        })
      }
    })

    this.bindersQuery.search(query).map(binder => {
      if (this.queriedItems.findIndex(item => item.key === binder.key) === -1) {
        this.queriedItems.push({
          title: binder.title.text,
          key: binder.key,
          description: binder.desc ? binder.desc.text : "",
          createdAt: "",
          onClick: () => {
            this.setSelectedBinder(binder)
            this.setCameFromSearch(true)
            navigate("/binder")
          },
        })
      }
    })
  }
}

decorate(BinderStore, {
  binders: observable,
  archivedBinders: observable,
  archiveBoard: action,
  archiveBinder: action,
  archivedBoards: observable,
  deletedBoards: observable,
  deletedBinders: observable,
  deleteBinder: action,
  deleteBoard: action,
  selectedBinder: observable,
  displayEditBinderModal: observable,
  displayEditBinderStyleModal: observable,
  displayAddBinderModal: observable,
  setSelectedBinder: action,
  setDisplayAddBinderModal: action,
  setDisplayEditBinderModal: action,
  setDisplayEditBinderStyleModal: action,
  updateBinder: action,
  addBinder: action,
  recoverBoardsFromArchiveByKeys: action,
  recoverBoardsFromTrashByKeys: action,
  recoverBoardsFromTrash: action,
  recoverBoardsFromArchive: action,
  definitelyDeleteBoardsByKeys: action,
  definitelyDeleteBoards: action,
  search: action,
  queriedItems: observable,
  cameFromSearch: observable,
  setCameFromSearch: action,
})

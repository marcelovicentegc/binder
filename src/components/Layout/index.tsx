/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import s from "./styles.module.scss"
import ThemeProvider from "../../assets/styles/ThemeProvider"
import classNames from "classnames"
import Image from "../Image"

interface IProps {
  children: React.ReactNode
  withOverflow?: boolean
  backgroundFilename?: string
}

export const Layout = ({
  children,
  withOverflow,
  backgroundFilename,
}: IProps) => {
  return (
    <>
      <ThemeProvider withOverflow={!!withOverflow} />
      <div className={s.appWrapper}>
        {backgroundFilename && (
          <Image filename={backgroundFilename} className={s.background} />
        )}
        <main
          className={classNames({
            [s.overflowless]: !withOverflow,
          })}
        >
          {children}
        </main>
      </div>
    </>
  )
}

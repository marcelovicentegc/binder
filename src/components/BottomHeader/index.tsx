import React from "react"
import classNames from "classnames"
import s from "./style.module.scss"

import { Link } from "gatsby"

interface IProps {
  selected: "trash" | "file" | "share"
}

export const BottomHeader = ({ selected }: IProps) => {
  return (
    <nav className={s.bottomHeaderNav}>
      <ul>
        <li
          className={classNames(s.trash, {
            [s.checked]: selected === "trash",
          })}
        >
          <Link to="/trash">Lixeira</Link>
        </li>
        <li
          className={classNames(s.file, {
            [s.checked]: selected === "file",
          })}
        >
          Arquivo
        </li>
        <li
          className={classNames(s.share, {
            [s.checked]: selected === "share",
          })}
        >
          Compartilhado
        </li>
      </ul>
    </nav>
  )
}

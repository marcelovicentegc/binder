import React from "react"
import Img from "gatsby-image"
import s from "./style.module.scss"
import classNames from "classnames"

import { StaticQuery, graphql } from "gatsby"
import { Legend2 } from "@binder/ui"

/*
 * This component is built using `gatsby-image` to automatically serve optimized
 * images with lazy loading and reduced file sizes. The image is loaded using a
 * `StaticQuery`, which allows us to load the image from directly within this
 * component, rather than having to pass the image data down from pages.
 *
 * For more information, see the docs:
 * - `gatsby-image`: https://gatsby.app/gatsby-image
 * - `StaticQuery`: https://gatsby.app/staticquery
 */

const Image: React.SFC<{
  filename: string
  className?: string
  legend?: string
  withBorderRadius?: boolean
}> = ({ filename, className, legend, withBorderRadius }) => {
  return (
    <StaticQuery
      query={graphql`
        query {
          allImageSharp {
            edges {
              node {
                fluid(maxWidth: 1200) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      `}
      render={data => {
        return (
          <div
            className={classNames(s.imageWrapper, {
              [s.withBorderRadius]: withBorderRadius,
            })}
          >
            <Img
              className={className}
              fluid={
                data.allImageSharp.edges.find(element => {
                  // Match string after final slash
                  return element.node.fluid.src.split("/").pop() === filename
                }).node.fluid
              }
            />
            {legend && <Legend2>{legend}</Legend2>}
          </div>
        )
      }}
    />
  )
}

export default Image

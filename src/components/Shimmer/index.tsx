import React from "react"
import s from "./style.module.scss"
import { Legend2, Legend } from "@binder/ui"
import Image from "../Image"
import classNames from "classnames"

interface IProps {
  title: string
  description: string
  createdAt: string
  horizontal: boolean
  selected: boolean
  luckyNumber: number
  onClick: () => void
}

export const Shimmer: React.FC<IProps> = ({
  title,
  description,
  createdAt,
  horizontal,
  onClick,
  selected,
  luckyNumber,
}) => {
  return (
    <div className={s.shimmerWrapper} onClick={onClick}>
      <div
        className={classNames(s.shimmer, {
          [s.horizontal]: horizontal,
          [s.selected]: selected,
        })}
      >
        <Image
          filename={
            (luckyNumber === 1 && !horizontal) ||
            (luckyNumber === 2 && !horizontal)
              ? "binderSketch1.png"
              : luckyNumber === 3 && !horizontal
              ? "binderSketch3.png"
              : luckyNumber === 4 && !horizontal
              ? "binderSketch4.png"
              : luckyNumber === 5 && !horizontal
              ? "binderSketch5.png"
              : luckyNumber === 6 && !horizontal
              ? "binderSketch7.png"
              : luckyNumber === 1 || luckyNumber === 3 || luckyNumber === 5
              ? "binderSketch6.png"
              : "binderSketch2.png"
          }
        />
      </div>
      <div className={s.details}>
        <Legend>{title}</Legend>
        <Legend2>{description}</Legend2>
        <Legend2>{createdAt}</Legend2>
      </div>
    </div>
  )
}

import React from "react"
import { OperationConfirmationModal, IOperationConfirmationModal } from ".."
import { TrashIcon } from "@binder/ui"
import { DeepOmit } from "../../../helpers/types"

export const DeletionConfirmationModal = ({
  onClickOutside,
  binderInformation,
  confirmButton,
  cancelButton,
}: DeepOmit<
  Omit<IOperationConfirmationModal, "question" | "extraInformation">,
  {
    cancelButton: {
      label: never
    }
    confirmButton: {
      label: never
      icon: never
    }
  }
>) => {
  return (
    <OperationConfirmationModal
      onClickOutside={onClickOutside}
      question={"Deseja mesmo excluir este fichário?"}
      extraInformation={
        "O fichário ficará na Lixeira por até 60 dias ou até a lixeira ser esvaziada."
      }
      binderInformation={binderInformation}
      cancelButton={{
        label: "Cancelar",
        onCancel: cancelButton.onCancel,
      }}
      confirmButton={{
        label: "Excluir fichário",
        onConfirm: confirmButton.onConfirm,
        icon: <TrashIcon />,
      }}
    />
  )
}

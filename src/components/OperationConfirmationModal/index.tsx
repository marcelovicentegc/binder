import React from "react"
import s from "./style.module.scss"
import classNames from "classnames"
import { Card } from "../Card"
import {
  Spotlight,
  Button,
  ButtonType,
  Label2,
  theme,
  BinderInterface,
  Label1,
} from "@binder/ui"

export interface IOperationConfirmationModal {
  onClickOutside: () => void
  question: string
  cancelButton: {
    label: string
    onCancel: () => void
  }
  confirmButton: {
    label: string
    icon: React.ReactNode
    onConfirm: () => void
  }
  extraInformation?: string
  binderInformation?: Omit<BinderInterface, "key" | "date" | "boards" | "img">
}

export const OperationConfirmationModal = ({
  onClickOutside,
  question,
  cancelButton,
  confirmButton,
  extraInformation,
  binderInformation,
}: IOperationConfirmationModal) => {
  return (
    <Card
      onClickOutside={onClickOutside}
      className={classNames(s.card, {
        [s.withExtraInformation]: extraInformation,
        [s.withExtraInformationAndBinderInformation]:
          extraInformation && binderInformation,
        [s.withBinderInformation]: binderInformation,
      })}
    >
      <Spotlight>{question}</Spotlight>
      {extraInformation && (
        <Label2
          style={{
            color: theme.color.gray3,
          }}
        >
          {extraInformation}
        </Label2>
      )}
      {binderInformation && (
        <>
          <div className={s.binderTitleRow}>
            <Label1
              style={{
                color: binderInformation.backgroundColor
                  ? binderInformation.backgroundColor
                  : binderInformation.title.color.startsWith("#F") ||
                    binderInformation.title.color.startsWith("white")
                  ? ""
                  : binderInformation.title.color,
              }}
            >
              {binderInformation.title.text}
              {"  "}
            </Label1>
            {binderInformation.desc && (
              <Label1
                style={{
                  fontWeight: 100,
                  color: binderInformation.backgroundColor
                    ? binderInformation.backgroundColor
                    : binderInformation.title.color.startsWith("#F") ||
                      binderInformation.title.color.startsWith("white")
                    ? ""
                    : binderInformation.title.color,
                }}
              >
                {binderInformation.desc.text}
              </Label1>
            )}
          </div>
          <div className={s.binderCourseRow}>
            {binderInformation.courseInformation && (
              <Label2 color={theme.color.gray4}>
                {binderInformation.courseInformation.grade} |{" "}
                {binderInformation.courseInformation.teacher}
              </Label2>
            )}
          </div>
        </>
      )}
      <div className={s.row}>
        <Button
          buttonType={ButtonType.secondary}
          label={cancelButton.label}
          onClick={cancelButton.onCancel}
        />
        <Button
          buttonType={ButtonType.secondary}
          label={confirmButton.label}
          icon={confirmButton.icon}
          onClick={confirmButton.onConfirm}
        />
      </div>
    </Card>
  )
}

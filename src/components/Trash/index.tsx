import React from "react"
import s from "./style.module.scss"
import { Layout2 } from "../Layout2"
import { navigate } from "gatsby"
import {
  MenuIcon,
  Menu,
  MenuItemsProps,
  RestoreIcon,
  TrashIcon,
  Button,
  ButtonType,
  XIcon,
  ButtonSize,
} from "@binder/ui"
import { rootStoreContext } from "../../stores/RootStore"
import { OperationConfirmationModal } from "../OperationConfirmationModal"
import { observer } from "mobx-react"

export const Trash = observer(() => {
  const { binderStore } = React.useContext(rootStoreContext)
  const [toggleMenu, setToggleMenu] = React.useState(false)
  const [
    displayRecoverBoardsFromTrashModal,
    setDisplayRecoverBoardsFromTrashModal,
  ] = React.useState(false)
  const [selectedItemsKeys, setSelectedItemsKeys] = React.useState<string[]>([])
  const [recoverSelectedBoards, setRecoverSelectedBoards] = React.useState(
    false
  )
  const [
    definitelyDeleteSelectedBoards,
    setDefinitelyDeleteSelectedBoards,
  ] = React.useState(false)
  const [definitelyDeleteBoards, setDefinitelyDeleteBoards] = React.useState(
    false
  )

  const menu: MenuItemsProps[] = [
    {
      icon: <RestoreIcon />,
      title: "Restaurar todas as pranchas",
      onClick: () => {
        setDefinitelyDeleteBoards(false)
        setDefinitelyDeleteSelectedBoards(false)
        setRecoverSelectedBoards(false)
        setDisplayRecoverBoardsFromTrashModal(true)
        setToggleMenu(false)
      },
    },
    {
      icon: <TrashIcon />,
      title: "Esvaziar lixeira",
      onClick: () => {
        setDefinitelyDeleteBoards(true)
        setDefinitelyDeleteSelectedBoards(false)
        setRecoverSelectedBoards(false)
        setDisplayRecoverBoardsFromTrashModal(true)
        setToggleMenu(false)
      },
    },
  ]

  const getDeletedBoards = () => {
    return binderStore.deletedBoards.map(board => {
      return {
        key: board.key,
        fk: board.fk,
        title: board.title,
        description: board.title,
        createdAt: board.date ? board.date.toString() : "",
      }
    })
  }

  const getDeletedBinders = () => {
    return binderStore.deletedBinders.map(binder => {
      return {
        key: binder.key,
        title: binder.title.text,
        description: binder.desc?.text,
        createdAt: binder.date ? binder.date.toString() : "",
      }
    })
  }
  return (
    <>
      {displayRecoverBoardsFromTrashModal && (
        <OperationConfirmationModal
          onClickOutside={() => setDisplayRecoverBoardsFromTrashModal(false)}
          question={
            recoverSelectedBoards
              ? "Deseja mesmo restaurar os itens selecionados?"
              : definitelyDeleteSelectedBoards
              ? "Deseja mesmo excluir os itens selecionados?"
              : definitelyDeleteBoards
              ? "Deseja mesmo esvaziar a lixeira?"
              : "Deseja mesmo restaurar todos os itens?"
          }
          extraInformation={
            (definitelyDeleteSelectedBoards || definitelyDeleteBoards) &&
            "Essa ação não poderá ser desfeita."
          }
          cancelButton={{
            label: "Cancelar",
            onCancel: () => setDisplayRecoverBoardsFromTrashModal(false),
          }}
          confirmButton={{
            label: recoverSelectedBoards
              ? "Restaurar itens selecionados"
              : definitelyDeleteSelectedBoards
              ? "Excluir permanentemente"
              : definitelyDeleteBoards
              ? "Esvaziar lixeira"
              : "Restaurar todos os itens",
            onConfirm: () => {
              if (recoverSelectedBoards) {
                binderStore.recoverBoardsFromTrashByKeys(selectedItemsKeys)
                setSelectedItemsKeys([])
              } else if (definitelyDeleteSelectedBoards) {
                binderStore.definitelyDeleteBoardsByKeys(selectedItemsKeys)
              } else if (definitelyDeleteBoards) {
                binderStore.definitelyDeleteBoards()
              } else {
                binderStore.recoverBoardsFromTrash()
              }

              setDisplayRecoverBoardsFromTrashModal(false)
            },
            icon:
              definitelyDeleteSelectedBoards || definitelyDeleteBoards ? (
                <TrashIcon />
              ) : (
                <RestoreIcon />
              ),
          }}
        />
      )}
      <Layout2
        backButtonProps={{ onClick: () => navigate("/binders") }}
        topLeftComponent={
          <div className={s.menuWrapper}>
            <MenuIcon onClick={() => setToggleMenu(!toggleMenu)} />
            <Menu
              menuItems={menu}
              topSpace={140}
              rightSpace={45}
              showMenu={toggleMenu}
            />
          </div>
        }
        title={"Lixeira"}
        extraFunctionalityBar={{
          elements: [
            <div className={s.row}>
              <Button
                label={"Restaurar seleção"}
                onClick={() => {
                  setDefinitelyDeleteSelectedBoards(false)
                  setRecoverSelectedBoards(true)
                  setDisplayRecoverBoardsFromTrashModal(true)
                }}
                buttonType={ButtonType.tertiary}
                icon={<RestoreIcon />}
              />
              <Button
                label={"Excluir seleção"}
                buttonType={ButtonType.tertiary}
                onClick={() => {
                  setRecoverSelectedBoards(false)
                  setDefinitelyDeleteSelectedBoards(true)
                  setDisplayRecoverBoardsFromTrashModal(true)
                }}
                icon={<TrashIcon />}
              />
            </div>,
            <Button
              label={"Cancelar seleção"}
              buttonType={ButtonType.secondary}
              buttonSize={ButtonSize.small}
              onClick={() => setSelectedItemsKeys([])}
              icon={<XIcon />}
            />,
          ],
          display: selectedItemsKeys.length > 0,
        }}
        sections={[
          {
            name: "Suas pranchas deletadas",
            items: getDeletedBoards(),
          },
          {
            name: "Seus fichários deletados",
            items: getDeletedBinders(),
          },
        ]}
        selectable={{
          selectedItemsKeys,
          setSelectedItemsKeys,
        }}
      />
    </>
  )
})

import React from "react"
import s from "./style.module.scss"
import {
  Button,
  BackArrowIcon,
  H1,
  Separator,
  ButtonType,
  Legend2,
  ButtonSize,
} from "@binder/ui"
import Image from "../../components/Image"
import { Shimmer } from "../Shimmer"
import { generateKey } from "../../helpers/generateKey"
import { generateNumberBetween } from "../../helpers/generateNumberBetween"
import { IGenericItem } from "../../stores/BinderStore"
import { observer } from "mobx-react"

interface IProps {
  backButtonProps: {
    onClick: () => void
  }
  topLeftComponent: React.ReactNode
  title: string
  filename?: string
  sections?: {
    name: string
    items: IGenericItem[]
  }[]
  selectable?: {
    setSelectedItemsKeys: (key: string[], fk?: string[]) => void
    selectedItemsKeys: string[]
  }
  extraFunctionalityBar?: {
    elements: React.ReactNode[]
    display: boolean
  }
}

export const Layout2: React.FC<IProps> = observer(
  ({
    backButtonProps,
    topLeftComponent,
    filename,
    title,
    sections,
    selectable,
    extraFunctionalityBar,
  }) => {
    return (
      <div className={s.container}>
        <div className={s.header}>
          <div className={s.row}>
            <Button
              label={"Voltar"}
              buttonType={ButtonType.secondary}
              icon={<BackArrowIcon />}
              onClick={backButtonProps.onClick}
              buttonSize={ButtonSize.small}
            />
          </div>
          <div className={s.row}>
            <H1>{title}</H1>
            {topLeftComponent}
          </div>
          <Separator gray />
        </div>
        {extraFunctionalityBar && extraFunctionalityBar.display && (
          <div className={s.row}>
            {extraFunctionalityBar.elements.map(functionality => (
              <div className={s.functionalityWrapper}>{functionality}</div>
            ))}
          </div>
        )}
        {sections &&
          sections.map((section, i) => {
            return (
              <section className={s.section} key={generateKey(20)}>
                {i > 0 && <Separator gray className={s.separator} />}
                <Legend2 className={s.legend}>{section.name}</Legend2>
                <div className={s.items}>
                  {section.items.map((item, itemIndex) => {
                    return (
                      <Shimmer
                        key={generateKey(20)}
                        luckyNumber={generateNumberBetween(1, 7)}
                        title={item.title}
                        description={item.description}
                        createdAt={item.description}
                        horizontal={itemIndex % 3 === 0 || itemIndex % 5 === 0}
                        onClick={() => {
                          if (item.onClick) {
                            item.onClick()
                          }

                          if (selectable) {
                            const isSelected =
                              selectable.selectedItemsKeys.findIndex(
                                selectedItemKey => selectedItemKey === item.key
                              ) > -1

                            if (isSelected) {
                              selectable.setSelectedItemsKeys([
                                ...selectable.selectedItemsKeys.filter(
                                  selectedItemKey =>
                                    selectedItemKey !== item.key
                                ),
                              ])
                            } else {
                              selectable.setSelectedItemsKeys([
                                ...selectable.selectedItemsKeys,
                                item.key,
                              ])
                            }
                          }
                        }}
                        selected={
                          selectable
                            ? selectable.selectedItemsKeys.findIndex(
                                selectedItemKey => selectedItemKey === item.key
                              ) > -1
                            : false
                        }
                      />
                    )
                  })}
                </div>
              </section>
            )
          })}
        {filename && (
          <div className={s.footer}>
            <Image filename={filename} />
          </div>
        )}
      </div>
    )
  }
)

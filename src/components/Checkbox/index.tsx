import React from "react"
import s from "./style.module.scss"
import { XIcon } from "@binder/ui"

interface IProps {
  label: string
  isChecked: boolean
  setIsChecked: (flag: boolean) => void
}

export const Checkbox = ({ label, setIsChecked, isChecked }: IProps) => {
  return (
    <div className={s.checkboxWrapper}>
      <div className={s.checkbox} onClick={() => setIsChecked(!isChecked)}>
        {isChecked && <XIcon />}
      </div>
      <div className={s.label}>{label}</div>
    </div>
  )
}

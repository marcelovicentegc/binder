import React from "react"
import classNames from "classnames"
import s from "./style.module.scss"
import Image from "../../../components/Image"

import { BinderStore } from "../../../stores"
import { CirclePicker } from "react-color"
import {
  Binder,
  Button,
  XIcon,
  ButtonType,
  SuccessArrowIcon,
  EditBinderCoverIcon,
  ArrowDownIcon,
  EditBinderThemeIcon,
  palette,
  BinderInterface,
  theme,
  Legend2,
} from "@binder/ui"

import leatherBg from "../../../assets/images/leatherBg.png"
import paperBg from "../../../assets/images/paperBg.png"
import kraftBg from "../../../assets/images/kraftBg.png"

interface IProps {
  currentBinder: BinderInterface
  binderStore: BinderStore
  setCurrentBinder: (currentBinder: BinderInterface) => void
}

export const EditBinderStyleModal = ({
  currentBinder,
  binderStore,
  setCurrentBinder,
}: IProps) => {
  const [showCoverStyleModal, setShowCoverStyleModal] = React.useState(false)
  const [showCoverColorModal, setShowCoverColorModal] = React.useState(false)

  const covers = [
    { type: "couro", image: "leatherBg.png" },
    { type: "papel", image: "paperBg.png" },
    { type: "kraft", image: "kraftBg.png" },
  ]

  return (
    <>
      <div className={s.buttonGroup}>
        <div className={s.topGroup}>
          <Button
            label={"Cancelar"}
            icon={<XIcon />}
            onClick={() => binderStore.setDisplayEditBinderStyleModal(false)}
          />
          <Button
            label={"Concluído"}
            buttonType={ButtonType.quaternary}
            icon={<SuccessArrowIcon color={"#4C4C4C"} />}
            onClick={() => binderStore.setDisplayEditBinderStyleModal(false)}
          />
        </div>
        <div className={s.bottomGroup}>
          <Button
            label={"Estilo de capa"}
            buttonType={ButtonType.secondary}
            icon={<EditBinderCoverIcon />}
            secondaryIcon={<ArrowDownIcon />}
            onClick={() => {
              setShowCoverColorModal(false)
              setShowCoverStyleModal(!showCoverStyleModal)
            }}
            style={{
              backgroundColor: showCoverStyleModal ? theme.color.white3 : "",
            }}
          />
          <Button
            // Aqui, é a "Cor da capa"
            label={"Cor do título"}
            buttonType={ButtonType.secondary}
            icon={<EditBinderThemeIcon />}
            secondaryIcon={<ArrowDownIcon />}
            onClick={() => {
              setShowCoverStyleModal(false)
              setShowCoverColorModal(!showCoverColorModal)
            }}
            style={{
              backgroundColor: showCoverColorModal ? theme.color.white3 : "",
            }}
          />
        </div>
        {showCoverStyleModal && (
          <div className={s.card}>
            <div className={s.title}>Capas simples</div>
            <div className={s.contentWrapper}>
              <div className={s.covers}>
                {covers.map(cover => (
                  <div className={s.cover}>
                    <Image
                      filename={cover.image}
                      legend={cover.type}
                      withBorderRadius
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
        {showCoverColorModal && (
          <div
            className={classNames(s.card, {
              [s.toTheRight]: true,
            })}
          >
            <div className={s.contentWrapper}>
              <div className={s.palette}>
                <CirclePicker
                  color={currentBinder.title.color}
                  colors={palette}
                  onChangeComplete={color =>
                    setCurrentBinder({
                      ...currentBinder,
                      title: {
                        ...currentBinder.title,
                        color: color.hex,
                      },
                    })
                  }
                />
              </div>
            </div>
          </div>
        )}
      </div>
      <div className={s.binderWrapper}>
        <Binder binder={currentBinder} disabled className={s.binder} />
      </div>
    </>
  )
}

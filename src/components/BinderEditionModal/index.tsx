import React from "react"
import s from "./style.module.scss"

import {
  Binder,
  Button,
  ButtonType,
  BackArrowIcon,
  SuccessArrowIcon,
  theme,
  EditBinderIcon,
  Input,
  InputType,
} from "@binder/ui"
import { BinderStore } from "../../stores"
import { observer } from "mobx-react"
import { EditBinderStyleModal } from "./EditBinderStyleModal"
import { BinderInterface } from "@binder/ui/lib/components/Binder"

interface IProps {
  binderStore: BinderStore
  currentBinder: BinderInterface
  mode: "edit" | "add"
  setCurrentBinder: (currentBinder: BinderInterface) => void
}

export const BinderEditionModal = observer(
  ({ binderStore, currentBinder, setCurrentBinder, mode }: IProps) => {
    return (
      <>
        {binderStore.displayEditBinderStyleModal ? (
          <EditBinderStyleModal
            currentBinder={currentBinder}
            binderStore={binderStore}
            setCurrentBinder={setCurrentBinder}
          />
        ) : (
          <>
            <div className={s.buttonGroup}>
              <Button
                label={"Voltar"}
                icon={<BackArrowIcon />}
                buttonType={ButtonType.secondary}
                onClick={() => {
                  mode === "edit"
                    ? binderStore.setDisplayEditBinderModal(false)
                    : binderStore.setDisplayAddBinderModal(false)
                }}
              />
              <Button
                label={"Salvar"}
                buttonType={ButtonType.secondary}
                icon={<SuccessArrowIcon color={theme.color.green3} />}
                onClick={() => {
                  if (mode === "edit") {
                    binderStore.updateBinder(currentBinder)
                    binderStore.setDisplayEditBinderModal(false)
                  } else {
                    binderStore.addBinder(currentBinder)
                    binderStore.setDisplayAddBinderModal(false)
                  }
                }}
              />
            </div>
            <Input
              label={"Disciplina"}
              type={InputType.secondary}
              inputProps={{
                type: "text",
                value: currentBinder.title.text,
                onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
                  setCurrentBinder({
                    ...currentBinder,
                    title: {
                      ...currentBinder.title,
                      text: e.target.value,
                    },
                  }),
              }}
            />
            <Input
              label={"Descrição"}
              type={InputType.secondary}
              inputProps={{
                type: "text",
                value: currentBinder.desc?.text,
                onChange: (e: React.ChangeEvent<HTMLInputElement>) =>
                  setCurrentBinder({
                    ...currentBinder,
                    desc: {
                      ...currentBinder.desc,
                      text: e.target.value,
                    },
                  }),
              }}
            />
            <div className={s.binderWrapper}>
              <Binder binder={currentBinder} disabled className={s.binder} />
              <Button
                label={"Personalizar fichário"}
                icon={<EditBinderIcon />}
                buttonType={ButtonType.secondary}
                onClick={() => binderStore.setDisplayEditBinderStyleModal(true)}
              />
            </div>
          </>
        )}
      </>
    )
  }
)

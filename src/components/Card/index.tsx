import React from "react"
import s from "./style.module.scss"
import classNames from "classnames"

interface IProps extends Omit<React.HTMLProps<HTMLDivElement>, "className"> {
  children: React.ReactNode
  onClickOutside: () => void
  backgroundless?: boolean
  className?: string
}

export const Card = ({
  children,
  onClickOutside,
  backgroundless,
  className,
  ...props
}: IProps) => {
  return (
    <>
      <div
        className={classNames({
          [s.cardBackground]: !backgroundless,
        })}
      />
      <div
        className={s.cardWrapper}
        onClick={e => e.target === e.currentTarget && onClickOutside()}
      >
        <div className={`${s.card} ${className}`} {...props}>
          {children}
        </div>
      </div>
    </>
  )
}

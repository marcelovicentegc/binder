import React from "react"
import { BinderEditor } from "@binder/editor"
import { navigate } from "gatsby"
import { rootStoreContext } from "../../stores/RootStore"

export const RichEditor = () => {
  const { binderStore } = React.useContext(rootStoreContext)

  return (
    <BinderEditor
      title={
        binderStore.selectedBinder ? binderStore.selectedBinder.title.text : ""
      }
      actions={{
        backButton: {
          label: "Voltar",
          buttonProps: {
            onClick: () => {
              binderStore.setCameFromSearch(false)
              navigate("/binder")
            },
          },
        },
      }}
      toolbarProps={{
        textColor: {
          menuTitle: "Cor do texto",
        },
        textBoxColor: {
          menuTitle: "Cor da caixa de texto",
        },
        textBoxStyle: {
          menuTitle: "Estilo da caixa de texto",
        },
      }}
    />
  )
}

import React from "react"
import s from "./style.module.scss"
import {
  MenuIcon,
  Menu,
  MenuItemsProps,
  FileIcon,
  Button,
  ButtonType,
  ButtonSize,
  XIcon,
  RestoreIcon,
} from "@binder/ui"
import { navigate } from "gatsby"
import { Layout2 } from "../../components/Layout2"
import { rootStoreContext } from "../../stores/RootStore"
import { OperationConfirmationModal } from "../OperationConfirmationModal"
import { observer } from "mobx-react"

export const Archive: React.FC = observer(() => {
  const { binderStore } = React.useContext(rootStoreContext)
  const [toggleMenu, setToggleMenu] = React.useState(false)
  const [
    displayUnarchiveConfirmationModal,
    setDisplayUnarchiveConfirmationModal,
  ] = React.useState(false)
  const [unarchiveSelection, setUnarchiveSelection] = React.useState(false)
  const [selectedItemsKeys, setSelectedItemsKeys] = React.useState<string[]>([])

  const menu: MenuItemsProps[] = [
    {
      icon: <RestoreIcon />,
      title: "Desarquivar todas as pranchas",
      onClick: () => {
        setUnarchiveSelection(false)
        setDisplayUnarchiveConfirmationModal(true)
        setToggleMenu(false)
      },
    },
  ]

  const getArchivedBoards = () => {
    return binderStore.archivedBoards.map(board => {
      return {
        key: board.key,
        fk: board.fk,
        title: board.title,
        description: board.title,
        createdAt: board.date ? board.date.toString() : "",
      }
    })
  }

  const getArchivedBinders = () => {
    return binderStore.archivedBinders.map(binder => {
      return {
        key: binder.key,
        title: binder.title.text,
        description: binder.desc?.text,
        createdAt: binder.date ? binder.date.toString() : "",
      }
    })
  }

  return (
    <>
      {displayUnarchiveConfirmationModal && (
        <OperationConfirmationModal
          onClickOutside={() => setDisplayUnarchiveConfirmationModal(false)}
          question={
            unarchiveSelection
              ? "Deseja mesmo desarquivar as pranchas selecionadas?"
              : "Deseja mesmo desarquivar todas as pranchas?"
          }
          cancelButton={{
            label: "Cancelar",
            onCancel: () => setDisplayUnarchiveConfirmationModal(false),
          }}
          confirmButton={{
            label: unarchiveSelection
              ? "Desarquivar selecionados"
              : "Desarquivar todas",
            onConfirm: () => {
              if (unarchiveSelection) {
                binderStore.recoverBoardsFromArchiveByKeys(selectedItemsKeys)
                setSelectedItemsKeys([])
              } else {
                binderStore.recoverBoardsFromArchive()
              }
              setDisplayUnarchiveConfirmationModal(false)
            },
            icon: <FileIcon />,
          }}
        />
      )}
      <Layout2
        backButtonProps={{ onClick: () => navigate("/binders") }}
        topLeftComponent={
          <div className={s.menuWrapper}>
            <MenuIcon onClick={() => setToggleMenu(!toggleMenu)} />
            <Menu
              menuItems={menu}
              topSpace={140}
              rightSpace={45}
              showMenu={toggleMenu}
            />
          </div>
        }
        title={"Arquivo"}
        extraFunctionalityBar={{
          elements: [
            <Button
              label={"Restaurar seleção"}
              onClick={() => {
                setUnarchiveSelection(true)
                setDisplayUnarchiveConfirmationModal(true)
              }}
              buttonType={ButtonType.tertiary}
              icon={<RestoreIcon />}
            />,
            <Button
              label={"Cancelar seleção"}
              buttonType={ButtonType.secondary}
              buttonSize={ButtonSize.small}
              onClick={() => setSelectedItemsKeys([])}
              icon={<XIcon />}
            />,
          ],
          display: selectedItemsKeys.length > 0,
        }}
        sections={[
          {
            name: "Suas pranchas arquivadas",
            items: getArchivedBoards(),
          },
          {
            name: "Seus fichários arquivados",
            items: getArchivedBinders(),
          },
        ]}
        selectable={{
          selectedItemsKeys,
          setSelectedItemsKeys,
        }}
      />
    </>
  )
})

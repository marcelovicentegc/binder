interface IStyle {
  [key: string]: string
}

declare module "*.scss" {
  const style: IStyle
  export = style
}

declare module "*.png" {
  const filePath: string
  export = filePath
}

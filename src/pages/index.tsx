import React from "react"

import { Layout } from "../components/Layout"
import SEO from "../components/SEO"
import { rootStoreContext, ECurrentStep } from "../stores/RootStore"
import { observer } from "mobx-react"
import { Intro } from "../containers/Intro"
import { navigate } from "gatsby"

const IndexPage = observer(() => {
  const { introStore } = React.useContext(rootStoreContext)

  return (
    <Layout>
      <SEO title="Home" />
      {introStore.currentStep === ECurrentStep.skip ? (
        navigate("/binders")
      ) : (
        <Intro />
      )}
    </Layout>
  )
})

export default IndexPage

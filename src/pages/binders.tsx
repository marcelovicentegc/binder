import React from "react"

import SEO from "../components/SEO"
import { Binders } from "../containers/Binders"
import { Layout } from "../components/Layout"
import { rootStoreContext } from "../stores/RootStore"
import { observer } from "mobx-react"

const BindersPage = observer(() => {
  const { binderStore } = React.useContext(rootStoreContext)

  return (
    <Layout
      withOverflow={
        !binderStore.displayAddBinderModal &&
        !binderStore.displayEditBinderModal
      }
    >
      <SEO title="Fichários" />
      <Binders />
    </Layout>
  )
})

export default BindersPage

import React from "react"

import { Layout } from "../components/Layout"
import SEO from "../components/SEO"
import { observer } from "mobx-react"
import { RichEditor } from "../components/RichEditor"
import { rootStoreContext } from "../stores/RootStore"

const Editor = observer(() => {
  const { editorStore } = React.useContext(rootStoreContext)

  return (
    <Layout>
      <SEO title="Editor de texto" />
      <RichEditor />
    </Layout>
  )
})

export default Editor

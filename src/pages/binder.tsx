import React from "react"
import SEO from "../components/SEO"
import { Layout } from "../components/Layout"
import { Boards } from "../containers/Boards"
import { rootStoreContext } from "../stores/RootStore"
import { observer } from "mobx-react"

const BinderPage = observer(() => {
  const { binderStore } = React.useContext(rootStoreContext)

  return (
    <Layout withOverflow={!binderStore.displayEditBinderModal}>
      <SEO title="Pranchas" />
      <Boards />
    </Layout>
  )
})

export default BinderPage

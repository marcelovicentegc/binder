import React from "react"

import { Layout } from "../components/Layout"
import SEO from "../components/SEO"
import { Archive } from "../components/Archive"

const ArchivePage = () => (
  <Layout withOverflow>
    <SEO title="Arquivo" />
    <Archive />
  </Layout>
)

export default ArchivePage

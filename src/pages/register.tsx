import React from "react"
import SEO from "../components/SEO"
import { Layout } from "../components/Layout"
import { observer } from "mobx-react"
import { Register } from "../containers/Register"

const RegisterPage = observer(() => {
  return (
    <Layout withOverflow>
      <SEO title="Cadastro" />
      <Register />
    </Layout>
  )
})

export default RegisterPage

import React from "react"

import { Layout } from "../components/Layout"
import SEO from "../components/SEO"
import { Trash } from "../components/Trash"

const TrashPage = () => (
  <Layout withOverflow>
    <SEO title="Lixeira" />
    <Trash />
  </Layout>
)

export default TrashPage

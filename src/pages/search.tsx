import React from "react"

import { Layout } from "../components/Layout"
import SEO from "../components/SEO"
import { Search } from "../containers/Search"

const SearchPage = () => (
  <Layout withOverflow>
    <SEO title="Busca" />
    <Search />
  </Layout>
)

export default SearchPage

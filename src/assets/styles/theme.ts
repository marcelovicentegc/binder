const theme = {
  fontFamily: [`"IBM Plex Sans"`].join(","),
}

export default theme

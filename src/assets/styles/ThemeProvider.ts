import { createGlobalStyle } from "@nfront/global-styles"

const ThemeProvider = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=IBM+Plex+Sans&display=swap');

  body {
    margin: 0;
    height: 100%;
    width: 100%;
    font-family: "IBM Plex Sans", sans-serif;
  }

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: rgba(0, 0, 0, 0.5);
     box-shadow: 0 0 1px rgba(255, 255, 255, 0.5);
  }
    

  ${(props: { withOverflow: boolean }) => {
    return props.withOverflow
      ? `
      html {
        min-height: 100vh;
        height: auto;
      }
    
      body {
        overflow-x: hidden;
        overflow-y: unset !important;
      }
    `
      : `
      html {
        height: 100vh;
        width: 100vw;
      }

      body {
        overflow: hidden;
      }
    `
  }}
`

export default ThemeProvider

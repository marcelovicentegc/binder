import React from "react"

import s from "./styles.module.scss"
import { rootStoreContext, ECurrentStep } from "../../../stores/RootStore"
import { Fade } from "../../Transitions/Fade"
import { Card } from "../../../components/Card"
import Image from "../../../components/Image"
import { navigate } from "gatsby"
import { BinderLogoIcon, Button, ButtonType } from "@binder/ui"

export const Login = () => {
  const [inProp, setInProp] = React.useState(false)
  React.useEffect(() => {
    setInProp(true)
  })
  const { introStore } = React.useContext(rootStoreContext)

  return (
    <Fade in={inProp} duration={800}>
      <Image filename={"authBg.png"} />
      <Card onClickOutside={() => null} backgroundless className={s.card}>
        <div className={s.login}>
          <div className={s.logoWrapper}>
            <BinderLogoIcon />
          </div>
          <Button
            label={"Entrar com o Google"}
            icon={
              <Image
                filename={"googleLogo.png"}
                className={s.socialNetworkLogo}
              />
            }
            buttonType={ButtonType.secondary}
            onClick={() => introStore.setCurrentStep(ECurrentStep.intro1)}
          />
          <Button
            label={"Entrar com o Facebook"}
            icon={
              <Image
                filename={"facebookLogo.png"}
                className={s.socialNetworkLogo}
              />
            }
            buttonType={ButtonType.secondary}
            onClick={() => introStore.setCurrentStep(ECurrentStep.intro1)}
          />
          <Button
            label={"Fazer login com email"}
            buttonType={ButtonType.secondary}
            onClick={() => introStore.setCurrentStep(ECurrentStep.intro1)}
            className={s.button}
          />
          <div className={s.registerWrapper}>
            <span>Primeiro acesso?</span>
            <pre> </pre>
            <Button
              label={"Cadastre-se aqui"}
              buttonType={ButtonType.quaternary}
              onClick={() => navigate("/register")}
            />
          </div>
        </div>
      </Card>
    </Fade>
  )
}

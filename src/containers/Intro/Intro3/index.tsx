import React from "react"
import s from "./style.module.scss"
import Image from "../../../components/Image"

export const Intro3 = () => {
  return (
    <div className={s.introWrapper}>
      <Image filename={"sketches1.png"} />
      <div className={s.inclinedRectangule} />
      <span>
        Construa <b>mapas mentais</b> sem bagunça, sem mistério e sem estresse.
      </span>
    </div>
  )
}

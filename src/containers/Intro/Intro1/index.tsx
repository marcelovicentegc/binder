import React from "react"
import s from "./style.module.scss"

export const Intro1 = () => {
  return (
    <div className={s.introWrapper}>
      <div className={s.inclinedRectangule} />
      <span>
        Um aplicativo para criação de <b>anotações escolares</b>.
      </span>
    </div>
  )
}

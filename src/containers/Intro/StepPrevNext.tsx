import React from "react"

interface IProps {
  classNames: string
}

export const StepPrevNext = (props: IProps) => {
  return (
    <svg
      width="12"
      height="21"
      viewBox="0 0 12 21"
      fill="none"
      className={props.classNames}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M9.19988 10.5L0 1.38686L1.40006 0L12 10.5L1.40006 21L0 19.6131L9.19988 10.5Z"
      />
    </svg>
  )
}

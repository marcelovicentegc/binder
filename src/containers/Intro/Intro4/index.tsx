import React from "react"
import s from "./style.module.scss"
import Image from "../../../components/Image"

export const Intro4 = () => {
  return (
    <div className={s.introWrapper}>
      <Image filename={"blackBinder.png"} />
      <div className={s.inclinedRectangule} />
      <span>
        Armazene, <b>organize, compartilhe</b> e imprima <b>suas anotações</b>.
      </span>
    </div>
  )
}

import React from "react"
import s from "./style.module.scss"
import Image from "../../../components/Image"

export const Intro2 = () => {
  return (
    <div className={s.introWrapper}>
      <Image filename={"blackBoard.png"} />
      <div className={s.inclinedRectangule} />
      <span>
        Escreva <b>resumos</b> organizados, lindos e interativos.
      </span>
    </div>
  )
}

import classNames from "classnames"
import React from "react"
import { rootStoreContext, ECurrentStep } from "../../stores/RootStore"
import { Login } from "./Login"
import { Intro1 } from "./Intro1"
import { Intro2 } from "./Intro2"
import s from "./style.module.scss"
import { Intro3 } from "./Intro3"
import { Intro4 } from "./Intro4"
import { StepPrevNext } from "./StepPrevNext"
import { Steps, Button } from "@binder/ui"

export const Intro = () => {
  const { introStore } = React.useContext(rootStoreContext)

  const goToStep = (step: number) => {
    introStore.setCurrentStep(step)
  }

  const goToNextStep = () => {
    const nextStep = introStore.currentStep + 1

    if (nextStep < Object.keys(ECurrentStep).length) {
      goToStep(nextStep)
    }
  }

  const goBack = () => {
    goToStep(introStore.currentStep - 1)
  }

  return (
    <>
      {introStore.currentStep === ECurrentStep.login && <Login />}
      {introStore.currentStep === ECurrentStep.intro1 && <Intro1 />}
      {introStore.currentStep === ECurrentStep.intro2 && <Intro2 />}
      {introStore.currentStep === ECurrentStep.intro3 && <Intro3 />}
      {introStore.currentStep === ECurrentStep.intro4 && <Intro4 />}
      {introStore.currentStep !== ECurrentStep.login && (
        <>
          <div
            className={s.buttonWrapper}
            onClick={() => introStore.setCurrentStep(ECurrentStep.skip)}
          >
            <Button
              label={
                introStore.currentStep === ECurrentStep.intro4
                  ? "Continuar"
                  : "Pular introdução"
              }
            />
          </div>
          {introStore.currentStep !== ECurrentStep.intro4 && (
            <div
              className={classNames(s.paddingStep, s.paddingStepNext)}
              onClick={goToNextStep}
            >
              <StepPrevNext classNames={s.nextButton} />
            </div>
          )}
          {introStore.currentStep !== ECurrentStep.intro1 && (
            <div
              className={classNames(s.paddingStep, s.paddingStepPrev)}
              onClick={goBack}
            >
              <StepPrevNext classNames={s.prevButton} />
            </div>
          )}
        </>
      )}
      {introStore.currentStep !== ECurrentStep.login && (
        <Steps
          currentStep={introStore.currentStep}
          totalSteps={(Object.keys(ECurrentStep).length - 2) / 2 - 1}
          setCurrentStep={currentStep => introStore.setCurrentStep(currentStep)}
        />
      )}
    </>
  )
}

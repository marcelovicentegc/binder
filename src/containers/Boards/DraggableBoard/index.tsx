import React from "react"
import { Draggable } from "react-beautiful-dnd"
import { Board } from "../Board"
import { BinderInterface, BoardInterface } from "@binder/ui"
import { generateKey } from "../../../helpers/generateKey"

interface IProps {
  index: number
  board: BoardInterface
  currentBinder: BinderInterface
  setCurrentBoard: (currentBoard: BoardInterface) => void
}

export const DraggableBoard: React.SFC<IProps> = ({
  index,
  board,
  setCurrentBoard,
  currentBinder,
}) => {
  return (
    <Draggable
      key={generateKey(20)}
      draggableId={`${index}-board`}
      index={index}
    >
      {provided => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          <Board
            board={board}
            key={index}
            setCurrentBoard={setCurrentBoard}
            currentBinder={currentBinder}
          />
        </div>
      )}
    </Draggable>
  )
}

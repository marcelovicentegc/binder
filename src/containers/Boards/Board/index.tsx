import classNames from "classnames"
import React, { useEffect, useRef, useState } from "react"
import s from "./style.module.scss"
import { navigate } from "gatsby"
import { rootStoreContext } from "../../../stores/RootStore"
import {
  BinderInterface,
  BoardInterface,
} from "@binder/ui/lib/components/Binder"
import {
  MenuItemsProps,
  MoveIcon,
  EditBinderIcon,
  TrashIcon,
  CustomContext,
} from "@binder/ui"

interface IProps {
  board: BoardInterface
  carousel?: boolean
  setCurrentBoard?: (currentBoard: BoardInterface) => void
  currentBinder?: BinderInterface
}

export const Board = ({
  board,
  carousel,
  setCurrentBoard,
  currentBinder,
}: IProps) => {
  const boardRef = useRef<HTMLDivElement>()
  const imgRef = useRef<HTMLImageElement>()
  const spanRef = useRef<HTMLSpanElement>()
  const [showContextMenu, setShowContextMenu] = useState(false)
  const { binderStore } = React.useContext(rootStoreContext)

  useEffect(() => {
    document.addEventListener("contextmenu", event => {
      event.preventDefault()
      if (
        event.target === boardRef.current ||
        event.target === imgRef.current ||
        event.target === spanRef.current
      ) {
        if (setCurrentBoard) {
          setCurrentBoard(board)
        }
        setShowContextMenu(true)
      } else {
        setShowContextMenu(false)
      }
    })

    document.addEventListener("click", event => {
      event.preventDefault()
      setShowContextMenu(false)
    })
  }, [])

  const contextMenu: MenuItemsProps[] = [
    {
      icon: <MoveIcon />,
      title: "Mover",
      onClick: () => {},
    },
    {
      icon: <EditBinderIcon />,
      title: "Arquivar",
      onClick: () => {
        binderStore.setDisplayEditBinderModal(true)
      },
    },
    {
      icon: <TrashIcon />,
      title: "Excluir",
      onClick: () => {
        if (setCurrentBoard) {
          setCurrentBoard(board)
          binderStore.deleteBoard(board.key, currentBinder)
        }
      },
    },
  ]

  return (
    <>
      <CustomContext showMenu={showContextMenu} menu={contextMenu} />
      <div
        className={classNames(s.board, { [s.carouselBoard]: carousel })}
        onClick={() => navigate("/board")}
        ref={boardRef}
      >
        <div className={s.boardSnapshotContainer} ref={imgRef}>
          {board.snapshot && (
            <img className={s.boardSnapshot} src={board.snapshot} />
          )}
        </div>
        <div className={s.boardTitleContainer}>
          <span className={s.boardTitle} ref={spanRef}>
            {board.title}
          </span>
        </div>
      </div>
    </>
  )
}

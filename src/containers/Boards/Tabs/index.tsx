import React from "react"
import s from "./style.module.scss"

export const Tabs = () => {
  return (
    <div className={s.boardTabs}>
      <div className={s.tabContainer}>
        <div className={s.tab}>
          <span>Pranchas recentes</span>
        </div>
      </div>
    </div>
  )
}

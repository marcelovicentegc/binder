import React, { useState, useEffect } from "react"
import s from "./style.module.scss"
import "pure-react-carousel/dist/react-carousel.es.css"

import { CarouselProvider, Slider, Slide, DotGroup } from "pure-react-carousel"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import { rootStoreContext } from "../../stores/RootStore"
import { observer } from "mobx-react"
import { Header } from "./Header"
import { Board } from "./Board"
import { Tabs } from "./Tabs"
import { generateKey } from "../../helpers/generateKey"
import { Card } from "../../components/Card"
import { BinderEditionModal } from "../../components/BinderEditionModal"
import {
  BinderInterface,
  BoardInterface,
} from "@binder/ui/lib/components/Binder"
import Image from "../../components/Image"
import { DraggableBoard } from "./DraggableBoard"

type boardOrganization =
  | "alphabeticalOrder"
  | "alphabeticalReverseOrder"
  | "timeOrder"
  | "timeReverseOrder"
  | "manualOrder"

type boardVisualization = "grade" | "sideByside"

export const Boards = observer(() => {
  const [mediaQueriesCarousel, setMediaQueriesCarousel] = useState()
  const [screenWidth, setScreenWidth] = useState(
    typeof window !== "undefined" ? window.innerWidth : ""
  )
  const { binderStore } = React.useContext(rootStoreContext)
  const [currentBinder, setCurrentBinder] = React.useState<BinderInterface>()
  const [boardOrder, setBoardOrder] = useState<boardOrganization>()
  const [board, setBoard] = useState()
  const [currentBoard, setCurrentBoard] = React.useState<BoardInterface>({
    key: generateKey(20),
    fk: "",
    title: "",
    snapshot: "",
  })
  const [boardVisualization, setBoardVisualization] = useState<
    boardVisualization
  >()
  React.useEffect(() => {
    setCurrentBinder(getCurrentBinder())
  }, [])

  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth)

      if (screenWidth < 500) {
        setMediaQueriesCarousel(1)
      } else if (screenWidth < 768) {
        setMediaQueriesCarousel(2)
      } else if (screenWidth < 1023) {
        setMediaQueriesCarousel(3)
      } else {
        setMediaQueriesCarousel(4)
      }
    }

    window.addEventListener("resize", handleResize)
    handleResize()

    return () => window.removeEventListener("resize", handleResize)
  }, [screenWidth])

  const getCurrentBinder = () => {
    return binderStore.selectedBinder
      ? binderStore.selectedBinder
      : binderStore.binders[0]
  }

  const order = {
    setAlphabeticalOrder: () => setBoardOrder("alphabeticalOrder"),
    setAlphabeticalReverseOrder: () =>
      setBoardOrder("alphabeticalReverseOrder"),
    setTimeOrder: () => setBoardOrder("timeOrder"),
    setTimeReverseOrder: () => setBoardOrder("timeReverseOrder"),
    setManualOrder: () => setBoardOrder("manualOrder"),
  }

  const visualization = {
    setSideBySideVisualization: () => setBoardVisualization("sideByside"),
    setGradeVisualization: () => setBoardVisualization("grade"),
  }

  const onDragEnd = result => {
    const { destination, source } = result

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return
    }

    const starredProjects = Object.assign(
      [],
      board ? board.starredProjects : currentBinder.boards
    )
    const project = board
      ? board.starredProjects[source.index]
      : currentBinder.boards[source.index]
    starredProjects.splice(source.index, 1)
    starredProjects.splice(destination.index, 0, project)
    setBoard({ starredProjects })
  }

  return (
    <>
      <Header orders={order} visualizations={visualization} />
      <div className={s.boardsWrapper}>
        {currentBinder &&
          currentBinder.boards &&
          currentBinder.boards.length > 0 && <Tabs />}
        {binderStore.displayEditBinderModal && (
          <Card
            onClickOutside={() => binderStore.setDisplayEditBinderModal(false)}
          >
            <BinderEditionModal
              currentBinder={currentBinder}
              setCurrentBinder={setCurrentBinder}
              binderStore={binderStore}
              mode={"edit"}
            />
          </Card>
        )}
        {(!currentBinder ||
          !currentBinder.boards ||
          currentBinder.boards.length === 0) && (
          <div className={s.noBoards}>
            <Image filename={"noBoardsFoundArrow.png"} className={s.arrow} />
            <Image filename={"noBoardsFoundFace.png"} className={s.face} />
            <span>Vamos criar uma nova prancha!</span>
          </div>
        )}
        <div className={s.boards}>
          {currentBinder &&
            currentBinder.boards &&
            (boardOrder === "alphabeticalOrder" ? (
              boardVisualization === "sideByside" ? (
                <CarouselProvider
                  naturalSlideWidth={203}
                  naturalSlideHeight={294}
                  totalSlides={currentBinder.boards.length}
                  infinite={true}
                  className={s.carousel}
                  visibleSlides={mediaQueriesCarousel}
                >
                  <Slider>
                    {currentBinder.boards
                      .sort((a, b) => {
                        if (a.title.toLowerCase() < b.title.toLowerCase())
                          return -1
                        if (a.title.toLowerCase() > b.title.toLowerCase())
                          return 1
                        return 0
                      })
                      .map((board, index) => (
                        <Slide key={index} index={index}>
                          <Board
                            carousel={true}
                            board={board}
                            key={generateKey(20)}
                            setCurrentBoard={setCurrentBoard}
                            currentBinder={currentBinder}
                          />
                        </Slide>
                      ))}
                  </Slider>
                  <DotGroup
                    showAsSelectedForCurrentSlideOnly={true}
                    disableActiveDots={false}
                  />
                </CarouselProvider>
              ) : (
                currentBinder.boards
                  .sort((a, b) => {
                    if (a.title.toLowerCase() < b.title.toLowerCase()) return -1
                    if (a.title.toLowerCase() > b.title.toLowerCase()) return 1
                    return 0
                  })
                  .map((board, index) => (
                    <Board
                      board={board}
                      key={index}
                      setCurrentBoard={setCurrentBoard}
                      currentBinder={currentBinder}
                    />
                  ))
              )
            ) : boardOrder === "alphabeticalReverseOrder" ? (
              boardVisualization === "sideByside" ? (
                <CarouselProvider
                  naturalSlideWidth={203}
                  naturalSlideHeight={294}
                  totalSlides={currentBinder.boards.length}
                  infinite={true}
                  className={s.carousel}
                  visibleSlides={mediaQueriesCarousel}
                >
                  <Slider>
                    {currentBinder.boards
                      .sort((a, b) => {
                        if (a.title.toLowerCase() < b.title.toLowerCase())
                          return 1
                        if (a.title.toLowerCase() > b.title.toLowerCase())
                          return -1
                        return 0
                      })
                      .map((board, index) => (
                        <Slide key={index} index={index}>
                          <Board
                            board={board}
                            carousel={true}
                            key={generateKey(20)}
                            setCurrentBoard={setCurrentBoard}
                            currentBinder={currentBinder}
                          />
                        </Slide>
                      ))}
                  </Slider>
                  <DotGroup
                    showAsSelectedForCurrentSlideOnly={true}
                    disableActiveDots={false}
                  />
                </CarouselProvider>
              ) : (
                currentBinder.boards
                  .sort((a, b) => {
                    if (a.title.toLowerCase() < b.title.toLowerCase()) return 1
                    if (a.title.toLowerCase() > b.title.toLowerCase()) return -1
                    return 0
                  })
                  .map((board, index) => (
                    <Board
                      board={board}
                      key={index}
                      setCurrentBoard={setCurrentBoard}
                      currentBinder={currentBinder}
                    />
                  ))
              )
            ) : boardOrder === "timeOrder" ? (
              boardVisualization === "sideByside" ? (
                <CarouselProvider
                  naturalSlideWidth={203}
                  naturalSlideHeight={294}
                  totalSlides={currentBinder.boards.length}
                  infinite={true}
                  className={s.carousel}
                  visibleSlides={mediaQueriesCarousel}
                >
                  <Slider>
                    {currentBinder.boards
                      .sort((a, b) => {
                        if (a.date > b.date) return -1
                        if (a.date < b.date) return 1
                        return 0
                      })
                      .map((board, index) => (
                        <Slide key={index} index={index}>
                          <Board
                            board={board}
                            carousel={true}
                            key={generateKey(20)}
                            setCurrentBoard={setCurrentBoard}
                            currentBinder={currentBinder}
                          />
                        </Slide>
                      ))}
                  </Slider>
                  <DotGroup
                    showAsSelectedForCurrentSlideOnly={true}
                    disableActiveDots={false}
                  />
                </CarouselProvider>
              ) : (
                currentBinder.boards
                  .sort((a, b) => {
                    if (a.date > b.date) return -1
                    if (a.date < b.date) return 1
                    return 0
                  })
                  .map((board, index) => (
                    <Board
                      board={board}
                      key={index}
                      setCurrentBoard={setCurrentBoard}
                      currentBinder={currentBinder}
                    />
                  ))
              )
            ) : boardOrder === "timeReverseOrder" ? (
              boardVisualization === "sideByside" ? (
                <CarouselProvider
                  naturalSlideWidth={203}
                  naturalSlideHeight={294}
                  totalSlides={currentBinder.boards.length}
                  infinite={true}
                  className={s.carousel}
                  visibleSlides={mediaQueriesCarousel}
                >
                  <Slider>
                    {currentBinder.boards
                      .sort((a, b) => {
                        if (a.date < b.date) return -1
                        if (a.date > b.date) return 1
                        return 0
                      })
                      .map((board, index) => (
                        <Slide key={index} index={index}>
                          <Board
                            board={board}
                            carousel={true}
                            key={generateKey(20)}
                            setCurrentBoard={setCurrentBoard}
                            currentBinder={currentBinder}
                          />
                        </Slide>
                      ))}
                  </Slider>
                  <DotGroup
                    showAsSelectedForCurrentSlideOnly={true}
                    disableActiveDots={false}
                  />
                </CarouselProvider>
              ) : (
                currentBinder.boards
                  .sort((a, b) => {
                    if (a.date < b.date) return -1
                    if (a.date > b.date) return 1
                    return 0
                  })
                  .map((board, index) => (
                    <Board
                      board={board}
                      key={index}
                      setCurrentBoard={setCurrentBoard}
                      currentBinder={currentBinder}
                    />
                  ))
              )
            ) : boardVisualization === "sideByside" ? (
              <CarouselProvider
                naturalSlideWidth={203}
                naturalSlideHeight={294}
                totalSlides={currentBinder.boards.length}
                infinite={true}
                className={s.carousel}
                visibleSlides={mediaQueriesCarousel}
              >
                <Slider>
                  {currentBinder.boards.map((board, index) => (
                    <Slide key={index} index={index}>
                      <Board
                        carousel={true}
                        board={board}
                        key={generateKey(20)}
                        setCurrentBoard={setCurrentBoard}
                        currentBinder={currentBinder}
                      />
                    </Slide>
                  ))}
                </Slider>
                <DotGroup
                  showAsSelectedForCurrentSlideOnly={true}
                  disableActiveDots={false}
                />
              </CarouselProvider>
            ) : boardOrder === "manualOrder" ? (
              <DragDropContext onDragEnd={result => onDragEnd(result)}>
                <Droppable droppableId="droppable" direction="horizontal">
                  {provided => (
                    <div
                      ref={provided.innerRef}
                      style={{ display: "flex", flexWrap: "wrap" }}
                      {...provided.droppableProps}
                    >
                      {board
                        ? board.starredProjects.map((board, index) => (
                            <DraggableBoard
                              index={index}
                              board={board}
                              currentBinder={currentBinder}
                              setCurrentBoard={setCurrentBoard}
                            />
                          ))
                        : currentBinder.boards.map((board, index) => (
                            <DraggableBoard
                              index={index}
                              board={board}
                              currentBinder={currentBinder}
                              setCurrentBoard={setCurrentBoard}
                            />
                          ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </DragDropContext>
            ) : (
              currentBinder.boards.map(board => (
                <Board
                  board={board}
                  key={generateKey(20)}
                  setCurrentBoard={setCurrentBoard}
                  currentBinder={currentBinder}
                />
              ))
            ))}
        </div>
      </div>
    </>
  )
})

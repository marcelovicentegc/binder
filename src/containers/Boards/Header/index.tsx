import classNames from "classnames"
import React, { useState } from "react"
import s from "./style.module.scss"
import { rootStoreContext } from "../../../stores/RootStore"
import { navigate } from "gatsby"
import { observer } from "mobx-react"
import {
  MenuItemsProps,
  AlphabeticalOrderIcon,
  AlphabeticalOrderReverseIcon,
  NewestIcon,
  OldestIcon,
  DragDropIcon,
  ArrowIcon,
  SearchIcon,
  VisualizationIcon,
  OrderingIcon,
  EditBinderIcon,
  MenuIcon,
  SideBySideIcon,
  GradeIcon,
  Button,
  Menu,
  BackArrowIcon,
  TrashIcon,
} from "@binder/ui"
import { Card } from "../../../components/Card"
import { OperationConfirmationModal } from "../../../components/OperationConfirmationModal"
import { DeletionConfirmationModal } from "../../../components/OperationConfirmationModal/DeletionConfirmationModal"

interface IOrderProps {
  setAlphabeticalOrder: (alphabeticalOrder: boolean) => void
  setAlphabeticalReverseOrder: (alphabeticalReverseOrder: boolean) => void
  setTimeOrder: (timeOrder: boolean) => void
  setTimeReverseOrder: (timeReverseOrder: boolean) => void
  setManualOrder: (manualOrder: boolean) => void
}

interface IVisualizationProps {
  setSideBySideVisualization: (sideBySideVisualization: boolean) => void
  setGradeVisualization: (gradeVisualization: boolean) => void
}

interface IProps {
  orders: IOrderProps
  visualizations: IVisualizationProps
}

export const Header = observer(({ orders, visualizations }: IProps) => {
  const { binderStore } = React.useContext(rootStoreContext)
  const [showMenu, setShowMenu] = useState(false)
  const [showMenuOrganization, setShowMenuOrganization] = useState(false)
  const [showMenuVisualization, setShowMenuVisualization] = useState(false)
  React.useEffect(() => {
    if (!binderStore.selectedBinder) {
      navigate("/binders")
    }
  }, [])
  const [alphabeticalOrder, setAlphabeticalOrder] = useState(false)
  const [alphabeticalOrderReverse, setAlphabeticalOrderReverse] = useState(
    false
  )
  const [oldest, setOldest] = useState(false)
  const [newest, setNewest] = useState(false)
  const [dragDrop, setDragDrop] = useState(false)
  const [
    displayBinderDeletionConfirmationModal,
    setDisplayBinderDeletionConfirmationModal,
  ] = React.useState(false)

  const menu: MenuItemsProps[] = [
    {
      icon: <VisualizationIcon />,
      onClick: () => setShowMenuVisualization(true),
      mode: "withArrow",
      title: "Visualização das pranchas",
    },
    {
      icon: <OrderingIcon />,
      onClick: () => setShowMenuOrganization(true),
      mode: ["section", "withArrow"],
      title: "Organização das pranchas",
    },
    {
      icon: <EditBinderIcon />,
      onClick: () => {
        setShowMenu(false)
        binderStore.setDisplayEditBinderModal(true)
      },
      mode: "withArrow",
      title: "Editar fichário",
    },
    {
      icon: <TrashIcon />,
      title: "Deletar fichário",
      onClick: () => {
        setDisplayBinderDeletionConfirmationModal(true)
        setShowMenu(false)
      },
    },
  ]

  const menuOrganization: MenuItemsProps[] = [
    {
      icon: <BackArrowIcon />,
      onClick: () => setShowMenuOrganization(false),
      title: "Organização das pranchas",
    },
    {
      icon: <AlphabeticalOrderIcon />,
      onClick: () => {
        orders.setAlphabeticalOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(true)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(false)
        setDragDrop(false)
      },
      title: "de A a Z",
      checkGreen: alphabeticalOrder,
    },
    {
      icon: <AlphabeticalOrderReverseIcon />,
      onClick: () => {
        orders.setAlphabeticalReverseOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(true)
        setOldest(false)
        setNewest(false)
        setDragDrop(false)
      },
      title: "de Z a A",
      checkGreen: alphabeticalOrderReverse,
    },
    {
      icon: <OldestIcon />,
      onClick: () => {
        orders.setTimeReverseOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(true)
        setNewest(false)
        setDragDrop(false)
      },
      title: "do mais antigo",
      checkGreen: oldest,
    },
    {
      icon: <NewestIcon />,
      onClick: () => {
        orders.setTimeOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(true)
        setDragDrop(false)
      },
      title: "do mais novo",
      checkGreen: newest,
    },
    {
      icon: <DragDropIcon />,
      onClick: () => {
        orders.setManualOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(false)
        setDragDrop(true)
      },
      title: "manual",
      checkGreen: dragDrop,
    },
  ]

  const menuVisualization: MenuItemsProps[] = [
    {
      icon: <BackArrowIcon />,
      onClick: () => setShowMenuVisualization(false),
      title: "Visualização das pranchas",
    },
    {
      icon: <SideBySideIcon />,
      onClick: () => {
        visualizations.setSideBySideVisualization(true)
        setShowMenu(false)
      },
      title: "Visualização lado a lado",
    },
    {
      icon: <GradeIcon />,
      onClick: () => {
        visualizations.setGradeVisualization(true)
        setShowMenu(false)
      },
      title: "Visualização em grade",
    },
  ]

  if (binderStore.selectedBinder) {
    return (
      <>
        {displayBinderDeletionConfirmationModal && (
          <DeletionConfirmationModal
            onClickOutside={() =>
              setDisplayBinderDeletionConfirmationModal(false)
            }
            binderInformation={{
              title: binderStore.selectedBinder.title,
              desc: binderStore.selectedBinder.desc,
              backgroundColor: binderStore.selectedBinder.backgroundColor,
              courseInformation: binderStore.selectedBinder
                .courseInformation ?? {
                grade: binderStore.selectedBinder.courseInformation.grade,
                teacher: binderStore.selectedBinder.courseInformation.teacher,
              },
            }}
            cancelButton={{
              onCancel: () => setDisplayBinderDeletionConfirmationModal(false),
            }}
            confirmButton={{
              onConfirm: () => {
                binderStore.deleteBinder(binderStore.selectedBinder.key)
                setDisplayBinderDeletionConfirmationModal(false)
                navigate("/binders")
              },
            }}
          />
        )}
        <div
          className={s.boardHeaderWrapper}
          style={{
            backgroundColor: binderStore.selectedBinder.backgroundColor,
          }}
        >
          <header
            className={classNames(s.boardHeader, {
              [s.backgroundless]: !binderStore.selectedBinder.img,
              [s.bottomBorderless]: !!binderStore.selectedBinder
                .backgroundColor,
            })}
          >
            <div className={s.topContainer}>
              <div className={s.top}>
                <div className={s.topLeft}>
                  <div className={s.back}>
                    <ArrowIcon color={binderStore.selectedBinder.title.color} />
                    <span
                      onClick={() =>
                        binderStore.cameFromSearch
                          ? navigate("/search")
                          : navigate("/binders")
                      }
                      style={{ color: binderStore.selectedBinder.title.color }}
                    >
                      Voltar
                    </span>
                  </div>
                </div>
                <div className={s.topRight}>
                  <SearchIcon color={binderStore.selectedBinder.title.color} />
                  <MenuIcon
                    onClick={() => {
                      setShowMenu(!showMenu)
                      setShowMenuOrganization(false)
                      setShowMenuVisualization(false)
                    }}
                    color={binderStore.selectedBinder.title.color}
                  />
                  <Menu
                    menuItems={
                      showMenuOrganization
                        ? menuOrganization
                        : showMenuVisualization
                        ? menuVisualization
                        : menu
                    }
                    showMenu={
                      showMenuOrganization || showMenuVisualization || showMenu
                    }
                    topSpace={75}
                    rightSpace={5}
                  />
                </div>
              </div>
              <div className={s.bottom}>
                <div className={s.bottomLeft}>
                  <span
                    style={{ color: binderStore.selectedBinder.title.color }}
                  >
                    {binderStore.selectedBinder.title.text}
                  </span>
                  {binderStore.selectedBinder.desc && (
                    <span
                      style={{ color: binderStore.selectedBinder.desc.color }}
                    >
                      {binderStore.selectedBinder.desc.text}
                    </span>
                  )}
                </div>
                <div className={s.bottomRight}>
                  <Button
                    label={"+ Nova prancha"}
                    onClick={() => navigate("/board")}
                  />
                </div>
              </div>
            </div>
            {binderStore.selectedBinder.img && (
              <img src={binderStore.selectedBinder.img} />
            )}
          </header>
        </div>
      </>
    )
  } else {
    return <div>Ooops, algo de errado aconteceu!</div>
  }
})

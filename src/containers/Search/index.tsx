import React from "react"
import { SearchBar } from "@binder/ui"
import { navigate } from "gatsby"
import { Layout2 } from "../../components/Layout2"
import { rootStoreContext } from "../../stores/RootStore"
import { observer } from "mobx-react"

export const Search = observer(() => {
  const { binderStore } = React.useContext(rootStoreContext)
  const [query, setQuery] = React.useState("binder")

  return (
    <Layout2
      backButtonProps={{ onClick: () => navigate("/binders") }}
      topLeftComponent={
        <SearchBar
          inputProps={{
            placeholder: "Nomes, pranchas, textos...",
            onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
              setQuery(e.target.value)
              setTimeout(() => binderStore.search(query), 1000)
            },
          }}
        />
      }
      filename={
        binderStore.queriedItems.length > 0
          ? undefined
          : "magnifyingGlasses.png"
      }
      title={"Busca"}
      sections={[
        {
          name: "Resultado",
          items: binderStore.queriedItems,
        },
      ]}
    />
  )
})

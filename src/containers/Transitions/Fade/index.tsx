import React from "react"
import { Transition } from "react-transition-group"

interface IProps {
  in?: boolean
  out?: boolean
  duration: number
  children: React.ReactNode
}

const defaultStyle = (duration: number, out: boolean) => {
  return {
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: out ? 1 : 0,
  }
}

const transitionStyles = {
  entering: { opacity: 1 },
  entered: { opacity: 1 },
  exiting: { opacity: 0 },
  exited: { opacity: 0 },
}

export const Fade = ({
  in: inProp,
  out: outProp,
  children,
  duration,
}: IProps) => {
  return (
    <Transition in={inProp} out={outProp} timeout={duration}>
      {state => (
        <div
          style={{
            ...defaultStyle(duration, !!outProp),
            ...transitionStyles[state],
          }}
        >
          {children}
        </div>
      )}
    </Transition>
  )
}

import React from "react"
import s from "./style.module.scss"
import { observer } from "mobx-react"
import { rootStoreContext, ECurrentStep } from "../../stores/RootStore"
import { navigate } from "gatsby"
import { dateMask } from "../../helpers/dateMask"
import { EShift, EGrade, IUser } from "../../stores/UserStore"
import { Terms } from "./Terms"
import { Checkbox } from "../../components/Checkbox"
import { TipsBar } from "./TipsBar"
import { Input } from "@binder/ui"

export const Register = observer(() => {
  const { introStore, userStore } = React.useContext(rootStoreContext)
  const [appOptions, setAppOptions] = React.useState({
    terms: false,
    subscribe: false,
  })
  const [isFocusedOn, setIsFocusedOn] = React.useState<keyof IUser | "terms">(
    "fullName"
  )
  React.useEffect(() => {
    userStore.clearUser()
  }, [])

  return (
    <div className={s.register}>
      <div className={s.header}>
        <p onClick={() => navigate("/")}>Cancelar</p>
        <h1>Cadastro</h1>
      </div>
      <form className={s.form}>
        <h2>Sobre você</h2>
        <div className={s.group}>
          <Input
            inputProps={{
              type: "text",
              placeholder: "nome e sobrenome",
              onFocus: () => setIsFocusedOn("fullName"),
              onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                userStore.setUser({
                  ...userStore.user,
                  fullName: e.target.value,
                })
              },
              value: userStore.user.fullName,
            }}
          />
          <div className={s.row}>
            <Input
              inputProps={{
                type: "text",
                placeholder: "data de nascimento",
                onFocus: () => setIsFocusedOn("birthdate"),
                onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                  userStore.setUser({
                    ...userStore.user,
                    birthdate: dateMask(e.target.value),
                  })
                },
                value: userStore.user.birthdate,
              }}
            />
            <Input
              inputProps={{
                type: "text",
                placeholder: "gênero",
                onFocus: () => setIsFocusedOn("gender"),
                onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                  userStore.setUser({
                    ...userStore.user,
                    gender: e.target.value,
                  })
                },
                value: userStore.user.gender,
              }}
            />
          </div>
        </div>
        <div className={s.group}>
          <Input
            inputProps={{
              type: "text",
              placeholder: "email",
              onFocus: () => setIsFocusedOn("email"),
              onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                userStore.setUser({
                  ...userStore.user,
                  email: e.target.value,
                })
              },
              value: userStore.user.email,
            }}
          />
          <Input
            inputProps={{
              type: "text",
              placeholder: "confirme o email",
              onFocus: () => setIsFocusedOn("email"),
              onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                const email = e.target.value

                if (email.length === userStore.user.email.length) {
                  // compare
                }
              },
            }}
          />
          <Input
            inputProps={{
              type: "text",
              placeholder: "nickname",
              onFocus: () => setIsFocusedOn("nickname"),
              onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                userStore.setUser({
                  ...userStore.user,
                  nickname: e.target.value,
                })
              },
              value: userStore.user.nickname,
            }}
          />
          <Input
            inputProps={{
              type: "password",
              placeholder: "senha",
              onFocus: () => setIsFocusedOn("password"),
            }}
          />
          <Input
            inputProps={{
              type: "password",
              placeholder: "confirme a senha",
              onFocus: () => setIsFocusedOn("password"),
            }}
          />
        </div>
        <h2>Sobre a escola</h2>
        <div className={s.group}>
          <Input
            inputProps={{
              type: "text",
              placeholder: "nome da Escola",
              onFocus: () => setIsFocusedOn("school"),
              onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                userStore.setUser({
                  ...userStore.user,
                  school: {
                    ...userStore.user.school,
                    name: e.target.value,
                  },
                })
              },
              value: userStore.user.school.name,
            }}
          />
          <div className={s.row}>
            <Input
              inputProps={{
                type: "text",
                placeholder: "série",
                onFocus: () => setIsFocusedOn("school"),
                onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                  userStore.setUser({
                    ...userStore.user,
                    school: {
                      ...userStore.user.school,
                      grade: EGrade.eighth,
                    },
                  })
                },
                value: userStore.user.school.grade,
              }}
            />
            <Input
              inputProps={{
                type: "text",
                placeholder: "turma",
                onFocus: () => setIsFocusedOn("school"),
                onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                  userStore.setUser({
                    ...userStore.user,
                    school: {
                      ...userStore.user.school,
                      class: e.target.value,
                    },
                  })
                },
                value: userStore.user.school.class,
              }}
            />
            <Input
              inputProps={{
                type: "texto",
                placeholder: "turno",
                onChange: (e: React.ChangeEvent<HTMLInputElement>) => {
                  setIsFocusedOn("school")
                  userStore.setUser({
                    ...userStore.user,
                    school: {
                      ...userStore.user.school,
                      shift: EShift.afternoon,
                    },
                  })
                },
                value: userStore.user.school.shift,
              }}
            />
          </div>
        </div>
        <h2>Sobre o aplicativo</h2>
        <div className={s.group}>
          <Terms />
          <Checkbox
            label={
              "Li e concordo com os Termos de Uso de Privacidade do aplicativo."
            }
            setIsChecked={() => {
              setIsFocusedOn("terms")
              setAppOptions({
                ...appOptions,
                terms: !appOptions.terms,
              })
            }}
            isChecked={appOptions.terms}
          />
          <Checkbox
            label={"Desejo receber dicas e novidades do Binder no meu email."}
            setIsChecked={() => {
              setIsFocusedOn("terms")
              setAppOptions({
                ...appOptions,
                subscribe: !appOptions.subscribe,
              })
            }}
            isChecked={appOptions.subscribe}
          />
        </div>
      </form>
      <TipsBar isFocusedOn={isFocusedOn} />
    </div>
  )
})

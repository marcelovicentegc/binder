import React from "react"
import s from "./style.module.scss"
import classNames from "classnames"
import { IUser } from "../../../stores/UserStore"
import { navigate } from "gatsby"
import { ECurrentStep } from "../../../stores"
import { rootStoreContext } from "../../../stores/RootStore"
import { Button, ButtonType, ArrowIcon } from "@binder/ui"

interface IProps {
  isFocusedOn: keyof IUser | "terms"
}

export const TipsBar = ({ isFocusedOn }: IProps) => {
  const { introStore } = React.useContext(rootStoreContext)

  return (
    <div className={s.progress}>
      <div
        className={classNames(s.spanWrapper, {
          [s.active]:
            isFocusedOn === "fullName" ||
            isFocusedOn === "birthdate" ||
            isFocusedOn === "gender",
        })}
      >
        <span>
          Queremos te conhecer melhor! Como podemos te chamar e quantos anos
          você tem? <br />
          👨‍💻 👦 👧
        </span>
      </div>
      <div
        className={classNames(s.spanWrapper, {
          [s.active]: isFocusedOn === "nickname",
        })}
      >
        <span>letras de A-Z e números</span>
      </div>
      <div
        className={classNames(s.spanWrapper, {
          [s.active]: isFocusedOn === "nickname",
        })}
      >
        <span>não são permitidos símbolos espaços ou caracteres especiais</span>
      </div>
      <div
        className={classNames(s.spanWrapper, {
          [s.active]: isFocusedOn === "school",
        })}
      >
        <span>🏫📚✏️</span>
      </div>
      <div
        className={classNames(s.spanWrapper, {
          [s.active]: isFocusedOn === "terms",
        })}
      >
        <span>👀✍️👨‍👨‍👧</span>
        <Button
          label={"Continuar"}
          onClick={() => {
            navigate("/")
            introStore.setCurrentStep(ECurrentStep.intro1)
          }}
          buttonType={ButtonType.round}
          icon={<ArrowIcon width={20} />}
        />
      </div>
    </div>
  )
}

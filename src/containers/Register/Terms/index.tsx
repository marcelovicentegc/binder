import React from "react"
import s from "./style.module.scss"

export const Terms = () => {
  return (
    <div className={s.termsWrapper}>
      <div className={s.terms}>
        <div className={s.title}>
          <span>
            <b>TERMOS E CONDIÇÕES GERAIS DE USO DO APLICATIVO "BINDER"</b>
          </span>
        </div>
        <div className={s.body}>
          <span>
            Estes termos e condições gerais de uso aplicam-se aos serviços pres-
            tados pela pessoa jurídica <b>BRIOCHE STUDIO</b>, devidamente
            registrada sob o CNPJ n. 12.345.678/001-99, com sede em: Avenida Pão
            Delícia, 9999 — Caminho de Migalhas de Pão, Padaria - BA. CEP:
            12345-099 por meio do aplicativo <b>Binder</b>.
          </span>
        </div>
      </div>
    </div>
  )
}

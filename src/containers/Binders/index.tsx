import classNames from "classnames"
import { CarouselProvider, Slider, Slide, DotGroup } from "pure-react-carousel"
import React, { useState, useEffect } from "react"
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import { Header } from "./Header"
import s from "./styles.module.scss"
import { rootStoreContext } from "../../stores/RootStore"
import {
  Binder,
  MenuItemsProps,
  FileIcon,
  EditBinderIcon,
  TrashIcon,
} from "@binder/ui"
import { observer } from "mobx-react"
import { BinderEditionModal } from "../../components/BinderEditionModal"
import { generateKey } from "../../helpers/generateKey"
import { Card } from "../../components/Card"
import { navigate } from "gatsby"
import { BinderInterface } from "@binder/ui/lib/components/Binder"
import { DeletionConfirmationModal } from "../../components/OperationConfirmationModal/DeletionConfirmationModal"

type binderOrganization =
  | "alphabeticalOrder"
  | "alphabeticalReverseOrder"
  | "timeOrder"
  | "timeReverseOrder"
  | "manualOrder"

type binderVisualization = "grade" | "sideByside"

export const Binders = observer(() => {
  const [screenWidth, setScreenWidth] = useState(
    typeof window !== "undefined" ? window.innerWidth : ""
  )
  const [mediaQueriesCarousel, setMediaQueriesCarousel] = useState()
  const { binderStore } = React.useContext(rootStoreContext)
  const [binderOrder, setBinderOrder] = useState<binderOrganization>()
  const [binder, setBinder] = useState()
  const [binderVisualization, setBinderVisualization] = useState<
    binderVisualization
  >()
  const [currentBinder, setCurrentBinder] = React.useState<BinderInterface>({
    key: generateKey(20),
    title: {
      text: "",
      color: "#000",
    },
  })
  const [
    displayDeletionConfirmationModal,
    setDisplayDeletionConfirmationModal,
  ] = React.useState(false)

  useEffect(() => {
    const handleResize = () => {
      setScreenWidth(window.innerWidth)

      if (screenWidth < 750) {
        setMediaQueriesCarousel(1)
      } else if (screenWidth < 1110) {
        setMediaQueriesCarousel(2)
      } else {
        setMediaQueriesCarousel(3)
      }
    }

    window.addEventListener("resize", handleResize)
    handleResize()

    return () => window.removeEventListener("resize", handleResize)
  }, [screenWidth])

  const order = {
    setAlphabeticalOrder: () => setBinderOrder("alphabeticalOrder"),
    setAlphabeticalReverseOrder: () =>
      setBinderOrder("alphabeticalReverseOrder"),
    setTimeOrder: () => setBinderOrder("timeOrder"),
    setTimeReverseOrder: () => setBinderOrder("timeReverseOrder"),
    setManualOrder: () => setBinderOrder("manualOrder"),
  }

  const visualization = {
    setSideBySideVisualization: () => setBinderVisualization("sideByside"),
    setGradeVisualization: () => setBinderVisualization("grade"),
  }

  const onDragEnd = result => {
    const { destination, source } = result

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return
    }

    const starredProjects = Object.assign(
      [],
      binder ? binder.starredProjects : binderStore.binders
    )
    const project = binder
      ? binder.starredProjects[source.index]
      : binderStore.binders[source.index]
    starredProjects.splice(source.index, 1)
    starredProjects.splice(destination.index, 0, project)
    setBinder({ starredProjects })
  }

  const handleOnClick = (binder: BinderInterface) => {
    binderStore.setSelectedBinder(binder)
    binderStore.setCameFromSearch(false)
    navigate("/binder")
  }

  const contextMenuItems: MenuItemsProps[] = [
    {
      icon: <FileIcon />,
      mode: "section",
      title: "Arquivar fichário",
      onClick: () => binderStore.archiveBinder(currentBinder.key),
    },
    {
      icon: <EditBinderIcon />,
      title: "Editar fichário",
      onClick: () => binderStore.setDisplayEditBinderModal(true),
    },
    {
      icon: <TrashIcon />,
      title: "Deletar fichário",
      onClick: () => setDisplayDeletionConfirmationModal(true),
    },
  ]

  return (
    <div className={s.container}>
      <Header orders={order} visualizations={visualization} />
      {(binderStore.displayAddBinderModal ||
        binderStore.displayEditBinderModal) && (
        <Card
          onClickOutside={() =>
            binderStore.displayAddBinderModal
              ? binderStore.setDisplayAddBinderModal(false)
              : binderStore.setDisplayEditBinderModal(false)
          }
        >
          <BinderEditionModal
            currentBinder={currentBinder}
            setCurrentBinder={setCurrentBinder}
            binderStore={binderStore}
            mode={binderStore.displayAddBinderModal ? "add" : "edit"}
          />
        </Card>
      )}
      {displayDeletionConfirmationModal && (
        <DeletionConfirmationModal
          onClickOutside={() => setDisplayDeletionConfirmationModal(false)}
          binderInformation={{
            title: currentBinder.title,
            desc: currentBinder.desc,
            backgroundColor: currentBinder.backgroundColor,
            courseInformation: currentBinder.courseInformation
              ? {
                  grade: currentBinder.courseInformation.grade,
                  teacher: currentBinder.courseInformation.teacher,
                }
              : undefined,
          }}
          cancelButton={{
            onCancel: () => setDisplayDeletionConfirmationModal(false),
          }}
          confirmButton={{
            onConfirm: () => {
              binderStore.deleteBinder(currentBinder.key)
              setDisplayDeletionConfirmationModal(false)
              navigate("/binders")
            },
          }}
        />
      )}
      <section
        className={classNames({
          [s.binders]:
            binderVisualization !== "sideByside" &&
            binderOrder !== "manualOrder",
        })}
      >
        {binderOrder === "alphabeticalOrder" ? (
          binderVisualization === "sideByside" ? (
            <CarouselProvider
              naturalSlideWidth={233}
              naturalSlideHeight={349}
              totalSlides={binderStore.binders.length}
              infinite={true}
              className={s.carousel}
              visibleSlides={mediaQueriesCarousel}
            >
              <Slider>
                {binderStore.binders
                  .sort((a, b) => {
                    if (a.title.text.toLowerCase() < b.title.text.toLowerCase())
                      return -1
                    if (a.title.text.toLowerCase() > b.title.text.toLowerCase())
                      return 1
                    return 0
                  })
                  .map((binder, index) => (
                    <Slide key={index} index={index}>
                      <Binder
                        binder={binder}
                        key={index}
                        carousel={true}
                        setCurrentBinder={setCurrentBinder}
                        contextMenu={contextMenuItems}
                      />
                    </Slide>
                  ))}
              </Slider>
              <DotGroup
                showAsSelectedForCurrentSlideOnly={true}
                disableActiveDots={false}
              />
            </CarouselProvider>
          ) : (
            binderStore.binders
              .sort((a, b) => {
                if (a.title.text.toLowerCase() < b.title.text.toLowerCase())
                  return -1
                if (a.title.text.toLowerCase() > b.title.text.toLowerCase())
                  return 1
                return 0
              })
              .map((binder, index) => {
                return (
                  <Binder
                    binder={binder}
                    key={index}
                    setCurrentBinder={setCurrentBinder}
                    onClick={() => handleOnClick(binder)}
                  />
                )
              })
          )
        ) : binderOrder === "alphabeticalReverseOrder" ? (
          binderVisualization === "sideByside" ? (
            <CarouselProvider
              naturalSlideWidth={233}
              naturalSlideHeight={349}
              totalSlides={binderStore.binders.length}
              infinite={true}
              className={s.carousel}
              visibleSlides={mediaQueriesCarousel}
            >
              <Slider>
                {binderStore.binders
                  .sort((a, b) => {
                    if (a.title.text.toLowerCase() < b.title.text.toLowerCase())
                      return 1
                    if (a.title.text.toLowerCase() > b.title.text.toLowerCase())
                      return -1
                    return 0
                  })
                  .map((binder, index) => {
                    return (
                      <Slide index={index} key={index}>
                        <Binder
                          binder={binder}
                          key={index}
                          carousel={true}
                          setCurrentBinder={setCurrentBinder}
                          onClick={() => handleOnClick(binder)}
                          contextMenu={contextMenuItems}
                        />
                      </Slide>
                    )
                  })}
              </Slider>
              <DotGroup
                showAsSelectedForCurrentSlideOnly={true}
                disableActiveDots={false}
              />
            </CarouselProvider>
          ) : (
            binderStore.binders
              .sort((a, b) => {
                if (a.title.text.toLowerCase() < b.title.text.toLowerCase())
                  return 1
                if (a.title.text.toLowerCase() > b.title.text.toLowerCase())
                  return -1
                return 0
              })
              .map((binder, index) => {
                return (
                  <Binder
                    binder={binder}
                    key={index}
                    setCurrentBinder={setCurrentBinder}
                    onClick={() => handleOnClick(binder)}
                    contextMenu={contextMenuItems}
                  />
                )
              })
          )
        ) : binderOrder === "timeOrder" ? (
          binderStore.binders
            .sort((a, b) => {
              if (a.date < b.date) return 1
              if (a.date > b.date) return -1
              return 0
            })
            .map((binder, index) => {
              return (
                <Binder
                  binder={binder}
                  key={index}
                  setCurrentBinder={setCurrentBinder}
                  onClick={() => handleOnClick(binder)}
                />
              )
            })
        ) : binderOrder === "timeReverseOrder" ? (
          binderVisualization === "sideByside" ? (
            <CarouselProvider
              naturalSlideWidth={233}
              naturalSlideHeight={349}
              totalSlides={binderStore.binders.length}
              infinite={true}
              className={s.carousel}
              visibleSlides={mediaQueriesCarousel}
            >
              <Slider>
                {binderStore.binders
                  .sort((a, b) => {
                    if (a.date < b.date) return -1
                    if (a.date > b.date) return 1
                    return 0
                  })
                  .map((binder, index) => {
                    return (
                      <Slide index={index} key={index}>
                        <Binder
                          binder={binder}
                          key={index}
                          carousel={true}
                          setCurrentBinder={setCurrentBinder}
                          onClick={() => handleOnClick(binder)}
                          contextMenu={contextMenuItems}
                        />
                      </Slide>
                    )
                  })}
              </Slider>
              <DotGroup
                showAsSelectedForCurrentSlideOnly={true}
                disableActiveDots={false}
              />
            </CarouselProvider>
          ) : (
            binderStore.binders
              .sort((a, b) => {
                if (a.date < b.date) return -1
                if (a.date > b.date) return 1
                return 0
              })
              .map((binder, index) => {
                return (
                  <Binder
                    binder={binder}
                    key={index}
                    setCurrentBinder={setCurrentBinder}
                    onClick={() => handleOnClick(binder)}
                    contextMenu={contextMenuItems}
                  />
                )
              })
          )
        ) : binderVisualization === "sideByside" ? (
          <CarouselProvider
            naturalSlideWidth={233}
            naturalSlideHeight={349}
            totalSlides={binderStore.binders.length}
            infinite={true}
            className={s.carousel}
            visibleSlides={mediaQueriesCarousel}
          >
            <Slider>
              {binderStore.binders.map((binder, index) => {
                return (
                  <Slide index={index} key={index}>
                    <Binder
                      binder={binder}
                      key={index}
                      carousel={true}
                      setCurrentBinder={setCurrentBinder}
                      onClick={() => handleOnClick(binder)}
                      contextMenu={contextMenuItems}
                    />
                  </Slide>
                )
              })}
            </Slider>
            <DotGroup
              showAsSelectedForCurrentSlideOnly={true}
              disableActiveDots={false}
            />
          </CarouselProvider>
        ) : binderOrder === "manualOrder" ? (
          <DragDropContext onDragEnd={result => onDragEnd(result)}>
            <Droppable droppableId="droppable" direction="horizontal">
              {provided => (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                  {binder
                    ? binder.starredProjects.map((binder, index) => {
                        return (
                          <Draggable
                            key={index}
                            draggableId={`${index}`}
                            index={index}
                          >
                            {provided => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                className={s.manual}
                              >
                                <Binder
                                  binder={binder}
                                  key={index}
                                  setCurrentBinder={setCurrentBinder}
                                  onClick={() => handleOnClick(binder)}
                                  contextMenu={contextMenuItems}
                                />
                              </div>
                            )}
                          </Draggable>
                        )
                      })
                    : binderStore.binders.map((binder, index) => {
                        return (
                          <Draggable
                            key={index}
                            draggableId={`${index}`}
                            index={index}
                          >
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                className={s.manual}
                              >
                                <Binder
                                  binder={binder}
                                  key={index}
                                  setCurrentBinder={setCurrentBinder}
                                  onClick={() => handleOnClick(binder)}
                                  contextMenu={contextMenuItems}
                                />
                              </div>
                            )}
                          </Draggable>
                        )
                      })}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        ) : (
          binderStore.binders.map((binder, index) => {
            return (
              <Binder
                binder={binder}
                key={index}
                setCurrentBinder={setCurrentBinder}
                onClick={() => handleOnClick(binder)}
                contextMenu={contextMenuItems}
              />
            )
          })
        )}
      </section>
    </div>
  )
})

import classNames from "classnames"
import React, { useState } from "react"
import s from "./styles.module.scss"
import { navigate } from "gatsby"
import { rootStoreContext, ECurrentStep } from "../../../stores/RootStore"
import {
  MenuItemsProps,
  NewIcon,
  SearchIcon,
  FileIcon,
  TrashIcon,
  ShareIcon,
  VisualizationIcon,
  OrderingIcon,
  BellIcon,
  UserOptionsIcon,
  ArrowIcon,
  EditUserIcon,
  AboutIcon,
  DoorIcon,
  AlphabeticalOrderIcon,
  AlphabeticalOrderReverseIcon,
  OldestIcon,
  NewestIcon,
  DragDropIcon,
  SideBySideIcon,
  GradeIcon,
  MenuIcon,
  Menu,
  BackArrowIcon,
} from "@binder/ui"

interface IOrderProps {
  setAlphabeticalOrder: (alphabeticalOrder: boolean) => void
  setAlphabeticalReverseOrder: (alphabeticalReverseOrder: boolean) => void
  setTimeOrder: (timeOrder: boolean) => void
  setTimeReverseOrder: (timeReverseOrder: boolean) => void
  setManualOrder: (manualOrder: boolean) => void
}

interface IVisualizationProps {
  setSideBySideVisualization: (sideBySideVisualization: boolean) => void
  setGradeVisualization: (gradeVisualization: boolean) => void
}

interface IProps {
  orders: IOrderProps
  visualizations: IVisualizationProps
}

export const Header = ({ orders, visualizations }: IProps) => {
  const { binderStore, introStore, userStore } = React.useContext(
    rootStoreContext
  )
  const [btnChecked, setBtnChecked] = useState(true)
  const [showMenu, setShowMenu] = useState(false)
  const [visibleMenuPerfil, setVisibleMenuPerfil] = useState(false)
  const [showMenuPerfil, setShowMenuPerfil] = useState(true)
  const [showMenuNotifications, setShowMenuNotifications] = useState(false)
  const [showMenuAccount, setShowMenuAccount] = useState(false)
  const [showMenuOrganization, setShowMenuOrganization] = useState(false)
  const [showMenuVisualization, setShowMenuVisualization] = useState(false)
  const [alphabeticalOrder, setAlphabeticalOrder] = useState(false)
  const [alphabeticalOrderReverse, setAlphabeticalOrderReverse] = useState(
    false
  )
  const [oldest, setOldest] = useState(false)
  const [newest, setNewest] = useState(false)
  const [dragDrop, setDragDrop] = useState(false)

  const menu: MenuItemsProps[] = [
    {
      icon: <NewIcon />,
      onClick: () => {
        setShowMenu(false)
        binderStore.setDisplayAddBinderModal(true)
      },
      mode: "section",
      title: "Novo fichário",
    },
    {
      icon: <SearchIcon />,
      onClick: () => navigate("/search"),
      title: "Pesquisar",
    },
    {
      icon: <FileIcon />,
      onClick: () => navigate("/archive"),
      title: "Arquivo",
    },
    {
      icon: <TrashIcon />,
      onClick: () => navigate("/trash"),
      title: "Lixeira",
    },
    {
      icon: <ShareIcon />,
      onClick: () => null,
      mode: "section",
      title: "Compartilhamento",
    },
    {
      icon: <VisualizationIcon />,
      onClick: () => setShowMenuVisualization(true),
      mode: "withArrow",
      title: "Visualização dos fichários",
    },
    {
      icon: <OrderingIcon />,
      onClick: () => setShowMenuOrganization(true),
      mode: "withArrow",
      title: "Organização dos fichários",
    },
  ]

  const notificationDescriptionPerfil = (
    <>
      <strong>Débora Benatti</strong> (@debsbntt) deseja compartilhar o resumo
      "Reforma Protestante" com você.
      <div className={s.notificationBtn}>
        <button
          className={classNames(s.accepted, { [s.checked]: btnChecked })}
          onClick={() => setBtnChecked(true)}
        >
          Aceitar
        </button>
        <button
          className={classNames(s.refused, { [s.checkedRefused]: !btnChecked })}
          onClick={() => setBtnChecked(false)}
        >
          Recusar
        </button>
      </div>
    </>
  )

  const notificationDescription = [
    <>
      <strong>
        <u>Débora Benatti</u> (@debsbntt)
      </strong>{" "}
      deseja compartilhar o resumo "Reforma Protestante" com você.
      <div className={s.notificationBtn}>
        <button
          className={classNames({ [s.checked]: btnChecked })}
          onClick={() => setBtnChecked(true)}
        >
          Aceitar
        </button>
        <button
          className={classNames({ [s.checked]: !btnChecked })}
          onClick={() => setBtnChecked(false)}
        >
          Recusar
        </button>
      </div>
    </>,
    <>
      <strong>
        <u>Jônio Carneiro</u> (@jocarn05)
      </strong>{" "}
      compartilhou com você o resumo "Fichamento – Mercantilismo".
    </>,
    <>
      <strong>
        <u>Débora Benatti</u> (@debsbntt)
      </strong>{" "}
      compartilhou com você a pesquisa "Trabalho sobre Jorge Amado".
    </>,
    <>
      <strong>
        <u>Débora Benatti</u> (@debsbntt)
      </strong>{" "}
      e{" "}
      <strong>
        <u>Jônio Carneiro</u> (jocarn05)
      </strong>{" "}
      aceitaram o convite para o mapa mental "Tipos de Planta: Comparação".
    </>,
    <>
      Bem vindo ao Binder! Para conhecer ainda mais o aplicativo, acesse, a
      qualquer momento, o Fichário Tutorial na tela inicial.
    </>,
  ]

  const menuPerfil: MenuItemsProps[] = [
    {
      descriptions: [notificationDescriptionPerfil],
      icon: <BellIcon />,
      mode: "withArrow",
      onClick: () => {
        setShowMenuNotifications(true)
        setShowMenuPerfil(false)
      },
      title: "Notificações",
    },
    {
      icon: <UserOptionsIcon className={s.userOptionsIcon} />,
      mode: "withArrow",
      onClick: () => {
        setShowMenuAccount(true)
        setShowMenuPerfil(false)
      },
      title: "Opções de conta",
    },
  ]

  const menuNotifications: MenuItemsProps[] = [
    {
      descriptions: notificationDescription,
      icon: <BackArrowIcon />,

      onClick: () => {
        setShowMenuNotifications(false)
        setShowMenuPerfil(true)
      },
      title: "Todas as notificações",
    },
  ]

  const menuAccount: MenuItemsProps[] = [
    {
      icon: <BackArrowIcon />,
      onClick: () => {
        setShowMenuAccount(false)
        setShowMenuPerfil(true)
      },
      title: "Opções de conta",
    },
    {
      icon: <EditUserIcon />,
      title: "Editar informações de conta",
    },
    {
      icon: <DoorIcon />,
      mode: "section",
      title: "Fazer logout",
      onClick: () => {
        introStore.setCurrentStep(ECurrentStep.login)
        navigate("/")
      },
    },
    {
      icon: <AboutIcon />,
      title: "Sobre o Binder",
    },
  ]

  const menuOrganization: MenuItemsProps[] = [
    {
      icon: <BackArrowIcon />,
      onClick: () => setShowMenuOrganization(false),
      title: "Organização dos fichários",
    },
    {
      icon: <AlphabeticalOrderIcon />,
      onClick: () => {
        orders.setAlphabeticalOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(true)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(false)
        setDragDrop(false)
      },
      title: "de A a Z",
      checkGreen: alphabeticalOrder,
    },
    {
      icon: <AlphabeticalOrderReverseIcon />,
      onClick: () => {
        orders.setAlphabeticalReverseOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(true)
        setOldest(false)
        setNewest(false)
        setDragDrop(false)
      },
      title: "de Z a A",
      checkGreen: alphabeticalOrderReverse,
    },
    {
      icon: <OldestIcon />,
      onClick: () => {
        orders.setTimeReverseOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(true)
        setNewest(false)
        setDragDrop(false)
      },
      title: "do mais antigo",
      checkGreen: oldest,
    },
    {
      icon: <NewestIcon />,
      onClick: () => {
        orders.setTimeOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(true)
        setDragDrop(false)
      },
      title: "do mais novo",
      checkGreen: newest,
    },
    {
      icon: <DragDropIcon />,
      onClick: () => {
        orders.setManualOrder(true)
        setShowMenu(false)
        setAlphabeticalOrder(false)
        setAlphabeticalOrderReverse(false)
        setOldest(false)
        setNewest(false)
        setDragDrop(true)
      },
      title: "manual",
      checkGreen: dragDrop,
    },
  ]

  const menuVisualization: MenuItemsProps[] = [
    {
      icon: <BackArrowIcon />,
      onClick: () => setShowMenuVisualization(false),
      title: "Visualização dos fichários",
    },
    {
      icon: <SideBySideIcon />,
      onClick: () => {
        visualizations.setSideBySideVisualization(true)
        setShowMenu(false)
      },
      title: "Visualização lado a lado",
    },
    {
      icon: <GradeIcon />,
      onClick: () => {
        visualizations.setGradeVisualization(true)
        setShowMenu(false)
      },
      title: "Visualização em grade",
    },
  ]

  return (
    <header className={s.binderHeader}>
      <div>
        <h1>Meus fichários</h1>
        <p>{userStore.user.fullName}</p>
      </div>
      <div className={s.navigation}>
        <MenuIcon
          onClick={() => {
            setShowMenu(!showMenu)
            setVisibleMenuPerfil(false)
            setShowMenuOrganization(false)
            setShowMenuVisualization(false)
          }}
        />
        <Menu
          menuItems={
            showMenuOrganization
              ? menuOrganization
              : showMenuVisualization
              ? menuVisualization
              : menu
          }
          topSpace={45}
          rightSpace={60}
          showMenu={showMenuOrganization || showMenuVisualization || showMenu}
        />
        <figure
          onClick={() => {
            setVisibleMenuPerfil(!visibleMenuPerfil)
            setShowMenu(false)
            setShowMenuOrganization(false)
            setShowMenuVisualization(false)
          }}
        >
          <img
            src="https://pbs.twimg.com/profile_images/962170088941019136/lgpCD8X4_400x400.jpg"
            alt="Name"
          />
          <figcaption>
            <span>1</span>
          </figcaption>
        </figure>
        <Menu
          menuItems={
            showMenuAccount
              ? menuAccount
              : showMenuPerfil
              ? menuPerfil
              : menuNotifications
          }
          topSpace={75}
          rightSpace={10}
          showMenu={visibleMenuPerfil}
          className={classNames(s.menuPerfil, {
            [s.notificationsMenu]:
              !showMenuAccount && !showMenuPerfil && visibleMenuPerfil,
          })}
        />
      </div>
    </header>
  )
}

# 📓 binder

[![Netlify Status](https://api.netlify.com/api/v1/badges/87b08df6-66fc-4d5c-8ea8-6dcc0874da4f/deploy-status)](https://app.netlify.com/sites/binder-prototype/deploys)

## ⬆️ Direções

1. Clone o repo: `git clone https://github.com/marcelovicentegc/binder.git`
2. cd nele: `cd binder`
3. Instale as dependências (use `npm`): `npm i`
4. Rode a aplicação: `npm start`

## 🚧 Desenvolvendo

### 🌊 Gitflow

`develop` ➡️ `feature` | `fix` | `test` | `refactor` ➡️ `develop` ➡️ `master`

## Recursos para desenvolvimento

[Projeto no Figma](https://www.figma.com/file/mzjEBqQDcCjVuXlQY32q80/Binder---Resumos-e-Mapas-Mentais?node-id=2%3A543)

[Projeto no Zeplin](https://app.zeplin.io/project/5df51e2605c6a316071e0399)

[Protótipo no Marvel](https://marvelapp.com/4bjhchi/screen/44596711)

[Notion](https://www.notion.so/b4e825707f7d41e4a2a8e5ab7e1aaf58?v=1981c78310db4182b7e8e0691a1a0af0)

module.exports = {
  siteMetadata: {
    title: `Binder`,
    description: `Uma ferramenta para criação de resumos escolares`,
    author: `Marcelo Cardoso e Valéria Almeida`,
  },
  pathPrefix: `/binder`,
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `minimal-ui`,
        // This is a temporary solution, as there is no proper favicon yet.
        // icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-global-styles`,
      options: {
        pathToConfigModule: `src/assets/styles/ThemeProvider`,
        props: {
          theme: `src/assets/styles/theme`,
        },
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-sass`,
    `gatsby-plugin-typescript`,
  ],
}

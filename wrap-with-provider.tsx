import React from "react"
import { Provider } from "mobx-react"
import { rootStore } from "./src/stores/RootStore"
import { ThemeProvider, theme } from "@binder/ui"

export default ({ element }) => (
  <Provider store={rootStore}>
    <ThemeProvider theme={theme}>{element}</ThemeProvider>
  </Provider>
)
